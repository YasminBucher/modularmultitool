package yasmin.modularmultitool.client;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import yasmin.modularmultitool.items.ModItems;

public class MultiToolItemGroup extends ItemGroup {
	public MultiToolItemGroup(String label) {
		super(label);
	}
	
	@Override
	public ItemStack createIcon() {
		return new ItemStack(ModItems.ITEM_MULTITOOL.get());
	}
}