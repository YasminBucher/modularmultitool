package yasmin.modularmultitool.recipes;

import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import yasmin.modularmultitool.ModularMultiTool;

public class ModRecipes {
	private static final DeferredRegister<IRecipeSerializer<?>> RECIPES = DeferredRegister.create(ForgeRegistries.RECIPE_SERIALIZERS, ModularMultiTool.MODID);
	public static final RegistryObject<MMTShapedRecipeSerializer> SHAPED_WITH_NBT = RECIPES.register("crafting_shaped_with_nbt", MMTShapedRecipeSerializer::new);
	
	public static void registerRecipeSerializers() {
		RECIPES.register(FMLJavaModLoadingContext.get().getModEventBus());
	}
}