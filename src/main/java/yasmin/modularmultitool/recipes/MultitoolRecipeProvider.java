package yasmin.modularmultitool.recipes;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.IFinishedRecipe;
import net.minecraft.data.RecipeProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import yasmin.modularmultitool.items.ModItems;
import yasmin.modularmultitool.items.base.ItemModuleBase;
import yasmin.modularmultitool.items.base.ItemToolModuleBase;
import yasmin.modularmultitool.items.modules.energy.ItemBatteryModule;
import yasmin.modularmultitool.util.ItemUtil;
import yasmin.modularmultitool.util.NbtUtil;

import javax.annotation.Nonnull;
import java.util.Objects;
import java.util.function.Consumer;

public class MultitoolRecipeProvider extends RecipeProvider {
	public MultitoolRecipeProvider(DataGenerator generatorIn) {
		super(generatorIn);
	}
	
	@Override
	protected void registerRecipes(@Nonnull Consumer<IFinishedRecipe> consumer) {
		//Multitool
		MMTShapedRecipeBuilder
				.shapedRecipe(new MMTCraftingResult(new ItemStack(ModItems.ITEM_MULTITOOL.get()), null))
				.patternLine("SpA")
				.patternLine("pcp")
				.patternLine("WpP")
				.key('p', MMTIngredient.fromItem(Items.OAK_PLANKS))
				.key('c', MMTIngredient.fromItem(Items.CHEST))
				.key('S', MMTIngredient.fromItem(Items.WOODEN_SHOVEL))
				.key('A', MMTIngredient.fromItem(Items.WOODEN_AXE))
				.key('W', MMTIngredient.fromItem(Items.WOODEN_SWORD))
				.key('P', MMTIngredient.fromItem(Items.WOODEN_PICKAXE))
				.addCriterion("has_oak_planks", this.hasItem(Items.OAK_PLANKS))
				.addCriterion("has_chest", this.hasItem(Items.CHEST))
				.addCriterion("has_wooden_shovel", this.hasItem(Items.WOODEN_SHOVEL))
				.addCriterion("has_wooden_axe", this.hasItem(Items.WOODEN_AXE))
				.addCriterion("has_wooden_sword", this.hasItem(Items.WOODEN_SWORD))
				.addCriterion("has_wooden_pickaxe", this.hasItem(Items.WOODEN_PICKAXE))
				.build(consumer);
		
		//Axe Module
		//Pickaxe Module
		//Shovel Module
		//Sword Module
		//Shears Module
		ModItems.ITEMS.getEntries().stream().filter(ro -> ro.get() instanceof ItemToolModuleBase).map(ro -> (ItemToolModuleBase) ro.get()).forEach(itmb -> MMTShapedRecipeBuilder
				.shapedRecipe(new MMTCraftingResult(new ItemStack(itmb), null))
				.patternLine("ppp")
				.patternLine("pTp")
				.patternLine("ppp")
				.key('T', MMTIngredient.fromItem(itmb.getBaseItem()))
				.key('p', MMTIngredient.fromItem(Items.OAK_PLANKS))
				.addCriterion("has_oak_planks", this.hasItem(Items.OAK_PLANKS))
				.build(consumer));
		
		//Autosmelt Module
		MMTShapedRecipeBuilder
				.shapedRecipe(new MMTCraftingResult(new ItemStack(ModItems.ITEM_MODULE_AUTOSMELT.get()), null))
				.patternLine("ppp")
				.patternLine("pTp")
				.patternLine("ppp")
				.key('T', MMTIngredient.fromItem(Items.MAGMA_CREAM))
				.key('p', MMTIngredient.fromItem(Items.MAGMA_BLOCK))
				.addCriterion("has_magma_cream", this.hasItem(Items.MAGMA_CREAM))
				.addCriterion("has_magma_block", this.hasItem(Items.MAGMA_BLOCK))
				.build(consumer);
		
		//Dig AOE Module
		MMTShapedRecipeBuilder
				.shapedRecipe(new MMTCraftingResult(new ItemStack(ModItems.ITEM_MODULE_DIGAOE.get()), null))
				.patternLine("ppp")
				.patternLine("pTp")
				.patternLine("ppp")
				.key('T', MMTIngredient.fromItem(Items.TNT))
				.key('p', MMTIngredient.fromItem(Items.FEATHER))
				.addCriterion("has_tnt", this.hasItem(Items.TNT))
				.addCriterion("has_feather", this.hasItem(Items.FEATHER))
				.build(consumer);
		
		//Dig Speed Module
		MMTShapedRecipeBuilder
				.shapedRecipe(new MMTCraftingResult(new ItemStack(ModItems.ITEM_MODULE_DIGSPEED.get()), null))
				.patternLine("prp")
				.patternLine("rTr")
				.patternLine("prp")
				.key('T', MMTIngredient.fromItem(Items.REPEATER))
				.key('p', MMTIngredient.fromItem(Items.REDSTONE))
				.key('r', MMTIngredient.fromItem(Items.REDSTONE_TORCH))
				.addCriterion("has_repeater", this.hasItem(Items.REPEATER))
				.addCriterion("has_redstone", this.hasItem(Items.REDSTONE))
				.addCriterion("has_redstone_torch", this.hasItem(Items.REDSTONE_TORCH))
				.build(consumer);
		
		//Fortune Module
		MMTShapedRecipeBuilder
				.shapedRecipe(new MMTCraftingResult(new ItemStack(ModItems.ITEM_MODULE_FORTUNE.get()), null))
				.patternLine("ppp")
				.patternLine("pTp")
				.patternLine("ppp")
				.key('T', MMTIngredient.fromItem(Items.BOOK))
				.key('p', MMTIngredient.fromItem(Items.FEATHER))
				.addCriterion("has_book", this.hasItem(Items.BOOK))
				.addCriterion("has_feather", this.hasItem(Items.FEATHER))
				.build(consumer);
		
		//Mining Power Module
		MMTShapedRecipeBuilder
				.shapedRecipe(new MMTCraftingResult(new ItemStack(ModItems.ITEM_MODULE_MININGPOWER.get()), null))
				.patternLine("ppp")
				.patternLine("pTp")
				.patternLine("ppp")
				.key('T', MMTIngredient.fromItem(Items.GOLDEN_PICKAXE))
				.key('p', MMTIngredient.fromItem(Items.IRON_BLOCK))
				.addCriterion("has_golden_pickaxe", this.hasItem(Items.GOLDEN_PICKAXE))
				.addCriterion("has_iron_block", this.hasItem(Items.IRON_BLOCK))
				.build(consumer);
		
		//Silktouch Module
		MMTShapedRecipeBuilder
				.shapedRecipe(new MMTCraftingResult(new ItemStack(ModItems.ITEM_MODULE_SILKTOUCH.get()), null))
				.patternLine("ppp")
				.patternLine("pTp")
				.patternLine("ppp")
				.key('T', MMTIngredient.fromItem(Items.ENDER_EYE))
				.key('p', MMTIngredient.fromItem(Items.GLOWSTONE_DUST))
				.addCriterion("has_ender_eye", this.hasItem(Items.ENDER_EYE))
				.addCriterion("has_glowstone_dust", this.hasItem(Items.GLOWSTONE_DUST))
				.build(consumer);
		
		//Weapon Damage Module
		MMTShapedRecipeBuilder
				.shapedRecipe(new MMTCraftingResult(new ItemStack(ModItems.ITEM_MODULE_WEAPONDAMAGE.get()), null))
				.patternLine("ppp")
				.patternLine("pTp")
				.patternLine("ppp")
				.key('T', MMTIngredient.fromItem(Items.GOLDEN_SWORD))
				.key('p', MMTIngredient.fromItem(Items.QUARTZ))
				.addCriterion("has_golden_sword", this.hasItem(Items.GOLDEN_SWORD))
				.addCriterion("has_quartz", this.hasItem(Items.QUARTZ))
				.build(consumer);
		
		//Battery Module
		MMTShapedRecipeBuilder
				.shapedRecipe(new MMTCraftingResult(new ItemStack(ModItems.ITEM_MODULE_BATTERY.get()), null))
				.patternLine("ppp")
				.patternLine("pTp")
				.patternLine("ppp")
				.key('T', MMTIngredient.fromItem(Items.REDSTONE_BLOCK))
				.key('p', MMTIngredient.fromItem(Items.MAGENTA_DYE))
				.addCriterion("has_redstone_block", this.hasItem(Items.REDSTONE_BLOCK))
				.addCriterion("has_magenta_dye", this.hasItem(Items.MAGENTA_DYE))
				.build(consumer);
		
		//Battery Module Level 1
		MMTShapedRecipeBuilder
				.shapedRecipe(new MMTCraftingResult(new ItemStack(ModItems.ITEM_MODULE_BATTERY.get()), NbtUtil.createNbtWithLevel(1)))
				.patternLine("pdp")
				.patternLine("eTe")
				.patternLine("pep")
				.key('T', MMTIngredient.fromItem(ModItems.ITEM_MODULE_BATTERY.get()).withNbt(NbtUtil.createNbtWithLevel(0)))
				.key('p', MMTIngredient.fromItem(Items.COMPARATOR))
				.key('e', MMTIngredient.fromItem(Items.REPEATER))
				.key('d', MMTIngredient.fromItem(Items.TRIDENT))
				.addCriterion("has_battery_module", this.hasItem(ModItems.ITEM_MODULE_BATTERY.get()))
				.addCriterion("has_comparator", this.hasItem(Items.COMPARATOR))
				.addCriterion("has_repeater", this.hasItem(Items.REPEATER))
				.addCriterion("has_trident", this.hasItem(Items.TRIDENT))
				.build(consumer, "_level1");
		
		//Battery Module Level 2
		MMTShapedRecipeBuilder
				.shapedRecipe(new MMTCraftingResult(new ItemStack(ModItems.ITEM_MODULE_BATTERY.get()), NbtUtil.createNbtWithLevel(2)))
				.patternLine("pdp")
				.patternLine("eTe")
				.patternLine("pep")
				.key('T', MMTIngredient.fromItem(ModItems.ITEM_MODULE_BATTERY.get()).withNbt(NbtUtil.createNbtWithLevel(1)))
				.key('p', MMTIngredient.fromItem(Items.PRISMARINE_SHARD))
				.key('e', MMTIngredient.fromItem(Items.PRISMARINE_CRYSTALS))
				.key('d', MMTIngredient.fromItem(Items.CONDUIT))
				.addCriterion("has_battery_module", this.hasItem(ModItems.ITEM_MODULE_BATTERY.get()))
				.addCriterion("has_prismarine_shard", this.hasItem(Items.PRISMARINE_SHARD))
				.addCriterion("has_prismarine_crystals", this.hasItem(Items.PRISMARINE_CRYSTALS))
				.addCriterion("has_conduit", this.hasItem(Items.CONDUIT))
				.build(consumer, "_level2");
		
		//Battery Module Level 3
		MMTShapedRecipeBuilder
				.shapedRecipe(new MMTCraftingResult(new ItemStack(ModItems.ITEM_MODULE_BATTERY.get()), NbtUtil.createNbtWithLevel(3)))
				.patternLine("pdp")
				.patternLine("eTe")
				.patternLine("pep")
				.key('T', MMTIngredient.fromItem(ModItems.ITEM_MODULE_BATTERY.get()).withNbt(NbtUtil.createNbtWithLevel(2)))
				.key('p', MMTIngredient.fromItem(Items.GHAST_TEAR))
				.key('e', MMTIngredient.fromItem(Items.BLAZE_ROD))
				.key('d', MMTIngredient.fromItem(Items.GOLDEN_APPLE))
				.addCriterion("has_battery_module", this.hasItem(ModItems.ITEM_MODULE_BATTERY.get()))
				.addCriterion("has_ghast_tear", this.hasItem(Items.GHAST_TEAR))
				.addCriterion("has_blaze_rod", this.hasItem(Items.BLAZE_ROD))
				.addCriterion("has_golden_apple", this.hasItem(Items.GOLDEN_APPLE))
				.build(consumer, "_level3");
		
		//Battery Module Level 4
		MMTShapedRecipeBuilder
				.shapedRecipe(new MMTCraftingResult(new ItemStack(ModItems.ITEM_MODULE_BATTERY.get()), NbtUtil.createNbtWithLevel(4)))
				.patternLine("pdp")
				.patternLine("eTe")
				.patternLine("pep")
				.key('T', MMTIngredient.fromItem(ModItems.ITEM_MODULE_BATTERY.get()).withNbt(NbtUtil.createNbtWithLevel(3)))
				.key('p', MMTIngredient.fromItem(Items.DIAMOND))
				.key('e', MMTIngredient.fromItem(Items.EMERALD))
				.key('d', MMTIngredient.fromItem(Items.MUSIC_DISC_MELLOHI))
				.addCriterion("has_battery_module", this.hasItem(ModItems.ITEM_MODULE_BATTERY.get()))
				.addCriterion("has_diamond", this.hasItem(Items.DIAMOND))
				.addCriterion("has_emerald", this.hasItem(Items.EMERALD))
				.addCriterion("has_music_disc_mellohi", this.hasItem(Items.MUSIC_DISC_MELLOHI))
				.build(consumer, "_level4");
		
		//Battery Module Level 5
		MMTShapedRecipeBuilder
				.shapedRecipe(new MMTCraftingResult(new ItemStack(ModItems.ITEM_MODULE_BATTERY.get()), NbtUtil.createNbtWithLevel(5)))
				.patternLine("pdp")
				.patternLine("eTe")
				.patternLine("pep")
				.key('T', MMTIngredient.fromItem(ModItems.ITEM_MODULE_BATTERY.get()).withNbt(NbtUtil.createNbtWithLevel(4)))
				.key('p', MMTIngredient.fromItem(Items.DIAMOND_BLOCK))
				.key('e', MMTIngredient.fromItem(Items.EMERALD_BLOCK))
				.key('d', MMTIngredient.fromItem(Items.DRAGON_EGG))
				.addCriterion("has_battery_module", this.hasItem(ModItems.ITEM_MODULE_BATTERY.get()))
				.addCriterion("has_diamond_block", this.hasItem(Items.DIAMOND_BLOCK))
				.addCriterion("has_emerald_block", this.hasItem(Items.EMERALD_BLOCK))
				.addCriterion("has_dragon_egg", this.hasItem(Items.DRAGON_EGG))
				.build(consumer, "_level5");
		
		//TODO Combat Laser Module
		//TODO Mining Laser Module
		//TODO Laser Range Module
		
		//All Module upgrades EXCEPT Battery Module upgrades
		ModItems.ITEMS.getEntries().stream().filter(ro -> ro.get() instanceof ItemModuleBase).map(ro -> (ItemModuleBase) ro.get()).filter(imb -> !(imb instanceof ItemBatteryModule)).filter(imb -> imb.getMaxLevel() > 0).forEach(imb -> {
			for(int i = 1; i <= imb.getMaxLevel(); i++) {
				MMTShapedRecipeBuilder
						.shapedRecipe(new MMTCraftingResult(new ItemStack(imb), NbtUtil.createNbtWithLevel(i)))
						.patternLine("UUU")
						.patternLine("UMU")
						.patternLine("UUU")
						.key('M', MMTIngredient.fromItem(imb).withNbt(NbtUtil.createNbtWithLevel(i - 1)))
						.key('U', MMTIngredient.fromItem(ItemUtil.getUpgradeItemForLevel(i)))
						.addCriterion("has_" + Objects.requireNonNull(ItemUtil.getUpgradeItemForLevel(i).getRegistryName()).toString(), this.hasItem(ItemUtil.getUpgradeItemForLevel(i)))
						.build(consumer, "_level" + i);
			}
		});
	}
}