package yasmin.modularmultitool.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipe;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.loot.LootContext;
import net.minecraft.loot.LootParameters;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import yasmin.modularmultitool.items.ItemMultiTool;
import yasmin.modularmultitool.items.modules.upgrades.ItemFortuneModule;
import yasmin.modularmultitool.util.ItemUtil;

import javax.annotation.Nonnull;
import java.util.*;

public class AutosmeltFakeBlock extends Block {
	protected static final Random RAND = new Random();
	
	public AutosmeltFakeBlock() {
		super(Block.Properties.create(Material.EARTH));
	}
	
	@Override
	public void onBlockExploded(BlockState state, World world, BlockPos pos, Explosion explosion) {
	
	}
	
	@Nonnull
	@Override
	public List<ItemStack> getDrops(BlockState state, LootContext.Builder builder) {
		if(state.hasProperty(AutosmeltFakeBlockInfoProperty.AFBI_PROP)) {
			AutosmeltFakeBlockInfo property = state.get(AutosmeltFakeBlockInfoProperty.AFBI_PROP);
			
			List<ItemStack> drops = property.getOriginalBlockState().getBlock().getDrops(property.getOriginalBlockState(), builder);
			List<ItemStack> newDrops = new ArrayList<>();
			
			for(ItemStack drop : drops) {
				Optional<FurnaceRecipe> optionalRecipe = builder.getWorld().getRecipeManager().getRecipe(IRecipeType.SMELTING, new Inventory(drop), builder.getWorld());
				
				if(optionalRecipe.isPresent()) {
					ItemStack dropSmelted = optionalRecipe.get().getRecipeOutput();
					
					if(dropSmelted.getCount() > 0) {
						dropSmelted = dropSmelted.copy();
						dropSmelted.setCount(drop.getCount());
						
						int fortuneLevel = 0;
						
						if(property.getItemStack().getItem() instanceof ItemMultiTool) {
							ItemStack fortuneModuleStack = ItemUtil.getInstalledModule(property.getItemStack(), ItemFortuneModule.class);
							
							if(fortuneModuleStack != null) {
								fortuneLevel = ItemUtil.getLevel(fortuneModuleStack);
							}
						}
						
						if(fortuneLevel > 0) {
							dropSmelted.setCount(dropSmelted.getCount() * RAND.nextInt(fortuneLevel + 1) + 1);
						}
						
						int xp = (int) optionalRecipe.get().getExperience();
						BlockPos pos = new BlockPos(builder.get(LootParameters.field_237457_g_));
						
						if(xp > 0 && pos != null) {
							state.getBlock().dropXpOnBlockBreak(builder.getWorld(), pos, xp);
						}
						
						newDrops.add(dropSmelted);
					} else {
						newDrops.add(drop);
					}
				} else {
					newDrops.add(drop);
				}
			}
			
			return newDrops;
		} else {
			return Collections.emptyList();
		}
	}
}