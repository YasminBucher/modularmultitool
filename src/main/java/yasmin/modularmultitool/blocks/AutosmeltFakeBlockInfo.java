package yasmin.modularmultitool.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.item.ItemStack;

import javax.annotation.Nonnull;

public class AutosmeltFakeBlockInfo implements Comparable<AutosmeltFakeBlockInfo> {
	public static final AutosmeltFakeBlockInfo ANY = new AutosmeltFakeBlockInfo(Blocks.AIR.getDefaultState(), ItemStack.EMPTY);
	
	private final BlockState originalBlockState;
	private final ItemStack itemStack;
	
	public AutosmeltFakeBlockInfo(BlockState originalBlockStateIn, ItemStack itemStackIn) {
		this.originalBlockState = originalBlockStateIn;
		this.itemStack = itemStackIn;
	}
	
	public BlockState getOriginalBlockState() {
		return this.originalBlockState;
	}
	
	public ItemStack getItemStack() {
		return this.itemStack;
	}
	
	@Override
	public int compareTo(@Nonnull AutosmeltFakeBlockInfo o) {
		if(o.equals(ANY))
			return 0;
		else if(this.originalBlockState.hashCode() == o.originalBlockState.hashCode())
			return this.itemStack.hashCode() - o.itemStack.hashCode();
		else
			return this.originalBlockState.hashCode() - o.originalBlockState.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof AutosmeltFakeBlockInfo))
			return false;
		
		AutosmeltFakeBlockInfo afbiObj = (AutosmeltFakeBlockInfo) obj;
		
		return afbiObj.originalBlockState == ANY.originalBlockState && afbiObj.itemStack == ANY.itemStack;
	}
}