package yasmin.modularmultitool.items.base;

import yasmin.modularmultitool.items.ModItems;

public class ItemModularToolBase extends ItemBase {
	public ItemModularToolBase() {
		super(ModItems.propertiesMultiTool);
	}
}