package yasmin.modularmultitool;

import net.minecraft.client.gui.ScreenManager;
import net.minecraft.data.DataGenerator;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemModelsProperties;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegisterCommandsEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import yasmin.modularmultitool.blocks.ModBlocks;
import yasmin.modularmultitool.client.MultiToolItemGroup;
import yasmin.modularmultitool.commands.CommandChargeEnergy;
import yasmin.modularmultitool.commands.CommandFakeRepair;
import yasmin.modularmultitool.commands.CommandInfo;
import yasmin.modularmultitool.gui.ModContainerTypes;
import yasmin.modularmultitool.gui.MultiToolContainerScreen;
import yasmin.modularmultitool.gui.MultiToolMachineContainerScreen;
import yasmin.modularmultitool.items.ModItems;
import yasmin.modularmultitool.machines.ModTileEntityTypes;
import yasmin.modularmultitool.recipes.ModRecipes;
import yasmin.modularmultitool.recipes.MultitoolRecipeProvider;
import yasmin.modularmultitool.util.ItemUtil;

@Mod(ModularMultiTool.MODID)
public class ModularMultiTool {
	public static final String MODID = "modularmultitool";
	public static final Logger LOGGER = LogManager.getLogger();
	
	public static ModularMultiTool INSTANCE;
	public static MultiToolItemGroup itemGroup = new MultiToolItemGroup("mmt_items");
	
	public ModularMultiTool() {
		INSTANCE = this;
		ModItems.registerItems();
		ModBlocks.registerBlocks();
		ModRecipes.registerRecipeSerializers();
		ModContainerTypes.registerContainerTypes();
		ModTileEntityTypes.registerTileEntityTypes();
		FMLJavaModLoadingContext.get().getModEventBus().addGenericListener(ContainerType.class, this::onContainerRegistry);
		
		//onGatherData is only called when runnung runData
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::onGatherData);
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::onClientSetup);
		
		MinecraftForge.EVENT_BUS.addListener(this::onRegisterCommands);
	}
	
	public void onContainerRegistry(final RegistryEvent.Register<ContainerType<?>> event) {
		ScreenManager.registerFactory(ModContainerTypes.CONTAINERTYPE_MULTITOOL.get(), MultiToolContainerScreen::new);
		ScreenManager.registerFactory(ModContainerTypes.CONTAINERTYPE_MULTITOOLMACHINE.get(), MultiToolMachineContainerScreen::new);
	}
	
	public void onGatherData(GatherDataEvent event) {
		DataGenerator gen = event.getGenerator();
		
		if(event.includeServer()) {
			gen.addProvider(new MultitoolRecipeProvider(gen));
		}
	}
	
	private void onClientSetup(FMLClientSetupEvent event){
		for(RegistryObject<Item> regItem : ModItems.ITEMS.getEntries()){
			ItemModelsProperties.registerProperty(regItem.get(), new ResourceLocation(MODID, "mmt_level"), (stack, world, entity) -> {
				int level = ItemUtil.getLevel(stack);
				return 0.2F * (float) level;
			});
		}
	}
	
	@OnlyIn(Dist.CLIENT)
	private void onRegisterCommands(RegisterCommandsEvent event){
		CommandChargeEnergy.register(event.getDispatcher());
		CommandFakeRepair.register(event.getDispatcher());
		CommandInfo.register(event.getDispatcher());
	}
}