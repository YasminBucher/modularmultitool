package yasmin.modularmultitool.blocks;

import com.google.common.collect.ImmutableSet;
import net.minecraft.state.Property;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Optional;

public class AutosmeltFakeBlockInfoProperty extends Property<AutosmeltFakeBlockInfo> {
	public static final AutosmeltFakeBlockInfoProperty AFBI_PROP = AutosmeltFakeBlockInfoProperty.create("afbi_prop");
	
	private static final ImmutableSet<AutosmeltFakeBlockInfo> ALLOWED_VALUES = ImmutableSet.of(AutosmeltFakeBlockInfo.ANY);
	
	protected AutosmeltFakeBlockInfoProperty(String name) {
		super(name, AutosmeltFakeBlockInfo.class);
	}
	
	public static AutosmeltFakeBlockInfoProperty create(String name) {
		return new AutosmeltFakeBlockInfoProperty(name);
	}
	
	@Nonnull
	@Override
	public Collection<AutosmeltFakeBlockInfo> getAllowedValues() {
		return ALLOWED_VALUES;
	}
	
	@Nonnull
	@Override
	public Optional<AutosmeltFakeBlockInfo> parseValue(@Nonnull String value) {
		return Optional.empty();
	}
	
	@Nonnull
	@Override
	public String getName(AutosmeltFakeBlockInfo value) {
		return value.toString();
	}
}
