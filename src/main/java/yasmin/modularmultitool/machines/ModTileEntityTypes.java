package yasmin.modularmultitool.machines;

import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import yasmin.modularmultitool.ModularMultiTool;
import yasmin.modularmultitool.blocks.ModBlocks;

public class ModTileEntityTypes {
	public static final DeferredRegister<TileEntityType<?>> TILEENTITYTYPES = DeferredRegister.create(ForgeRegistries.TILE_ENTITIES, ModularMultiTool.MODID);
	public static final RegistryObject<TileEntityType<?>> MULTITOOLMACHINE = TILEENTITYTYPES.register("tet_multitoolnachine", () -> TileEntityType.Builder.create(MultiToolMachineTileEntity::new, ModBlocks.MULTITOOLMACHINE.get()).build(null));
	
	public static void registerTileEntityTypes() {
		TILEENTITYTYPES.register(FMLJavaModLoadingContext.get().getModEventBus());
	}
}