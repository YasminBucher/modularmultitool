package yasmin.modularmultitool.items.modules.upgrades;

import yasmin.modularmultitool.config.Settings;
import yasmin.modularmultitool.items.base.ItemUpgradeModuleBase;

public class ItemWeaponDamageModule extends ItemUpgradeModuleBase {
	public ItemWeaponDamageModule() {
		super();
	}
	
	@Override
	public boolean isEnabled() {
		return Settings.itemWeaponDamageModule;
	}
}