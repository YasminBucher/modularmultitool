package yasmin.modularmultitool.items.modules.upgrades;

import yasmin.modularmultitool.config.Settings;
import yasmin.modularmultitool.items.base.ItemUpgradeModuleBase;

public class ItemSilktouchModule extends ItemUpgradeModuleBase {
	public ItemSilktouchModule() {
		super();
	}
	
	@Override
	public boolean isEnabled() {
		return Settings.itemSilktouchModule;
	}
	
	@Override
	public int getMaxLevel() {
		return 0;
	}
}