package yasmin.modularmultitool.items.modules.upgrades;

import yasmin.modularmultitool.config.Settings;
import yasmin.modularmultitool.items.base.ItemUpgradeModuleBase;

public class ItemFortuneModule extends ItemUpgradeModuleBase {
	public ItemFortuneModule() {
		super();
	}
	
	@Override
	public boolean isEnabled() {
		return Settings.itemFortuneModule;
	}
	
	@Override
	public int getMaxLevel() {
		return 2;
	}
}