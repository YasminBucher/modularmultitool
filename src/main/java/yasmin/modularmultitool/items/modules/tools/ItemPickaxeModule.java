package yasmin.modularmultitool.items.modules.tools;

import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraftforge.common.ToolType;
import yasmin.modularmultitool.config.Settings;
import yasmin.modularmultitool.items.base.ItemToolModuleBase;

public class ItemPickaxeModule extends ItemToolModuleBase {
	public ItemPickaxeModule() {
		super(ToolType.PICKAXE, 5.0F);
	}
	
	@Override
	public Item getBaseItem() {
		return Items.WOODEN_PICKAXE;
	}
	
	@Override
	public boolean isEnabled() {
		return Settings.itemPickaxeModule;
	}
}