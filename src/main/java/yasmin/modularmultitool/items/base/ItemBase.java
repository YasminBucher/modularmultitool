package yasmin.modularmultitool.items.base;

import net.minecraft.item.Item;
import yasmin.modularmultitool.ModularMultiTool;

public class ItemBase extends Item {
	public ItemBase(Properties props) {
		super(props.group(ModularMultiTool.itemGroup));
	}
	
	public boolean isEnabled() {
		return false;
	}
}