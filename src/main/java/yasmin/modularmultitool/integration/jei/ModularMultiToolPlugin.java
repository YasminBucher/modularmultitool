package yasmin.modularmultitool.integration.jei;

import mezz.jei.api.IModPlugin;
import mezz.jei.api.JeiPlugin;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.ingredients.subtypes.ISubtypeInterpreter;
import mezz.jei.api.registration.IModIngredientRegistration;
import mezz.jei.api.registration.IRecipeRegistration;
import mezz.jei.api.registration.ISubtypeRegistration;
import mezz.jei.api.runtime.IJeiRuntime;
import net.minecraft.client.Minecraft;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import yasmin.modularmultitool.ModularMultiTool;
import yasmin.modularmultitool.items.ModItems;
import yasmin.modularmultitool.items.base.ItemModuleBase;
import yasmin.modularmultitool.recipes.MMTIngredient;
import yasmin.modularmultitool.recipes.MMTShapedRecipe;
import yasmin.modularmultitool.util.ItemUtil;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@JeiPlugin
public class ModularMultiToolPlugin implements IModPlugin {
	public static final MMTIngredientRenderer INGREDIENT_RENDERER = new MMTIngredientRenderer();
	public static final MMTIngredientHelper INGREDIENT_HELPER = new MMTIngredientHelper();
	
	private static final ISubtypeInterpreter MODULE_INTERPRETER = itemStack -> {
		Item item = itemStack.getItem();
		
		if(item instanceof ItemModuleBase) {
			int level = ItemUtil.getLevel(itemStack);
			return "Level " + level;
		}
		
		return ISubtypeInterpreter.NONE;
	};
	
	@Nonnull
	@Override
	public ResourceLocation getPluginUid() {
		return new ResourceLocation(ModularMultiTool.MODID, "modularmultitool");
	}
	
	@Override
	public void registerItemSubtypes(ISubtypeRegistration registration) {
		registration.registerSubtypeInterpreter(ModItems.ITEM_MODULE_PICKAXE.get(), MODULE_INTERPRETER);
		registration.registerSubtypeInterpreter(ModItems.ITEM_MODULE_AXE.get(), MODULE_INTERPRETER);
		registration.registerSubtypeInterpreter(ModItems.ITEM_MODULE_SHOVEL.get(), MODULE_INTERPRETER);
		registration.registerSubtypeInterpreter(ModItems.ITEM_MODULE_SWORD.get(), MODULE_INTERPRETER);
		registration.registerSubtypeInterpreter(ModItems.ITEM_MODULE_SHEARS.get(), MODULE_INTERPRETER);
		registration.registerSubtypeInterpreter(ModItems.ITEM_MODULE_LASER_COMBAT.get(), MODULE_INTERPRETER);
		registration.registerSubtypeInterpreter(ModItems.ITEM_MODULE_LASER_MINING.get(), MODULE_INTERPRETER);
		registration.registerSubtypeInterpreter(ModItems.ITEM_MODULE_AUTOSMELT.get(), MODULE_INTERPRETER);
		registration.registerSubtypeInterpreter(ModItems.ITEM_MODULE_DIGAOE.get(), MODULE_INTERPRETER);
		registration.registerSubtypeInterpreter(ModItems.ITEM_MODULE_DIGSPEED.get(), MODULE_INTERPRETER);
		registration.registerSubtypeInterpreter(ModItems.ITEM_MODULE_LASERRANGE.get(), MODULE_INTERPRETER);
		registration.registerSubtypeInterpreter(ModItems.ITEM_MODULE_MININGPOWER.get(), MODULE_INTERPRETER);
		registration.registerSubtypeInterpreter(ModItems.ITEM_MODULE_SILKTOUCH.get(), MODULE_INTERPRETER);
		registration.registerSubtypeInterpreter(ModItems.ITEM_MODULE_WEAPONDAMAGE.get(), MODULE_INTERPRETER);
		registration.registerSubtypeInterpreter(ModItems.ITEM_MODULE_FORTUNE.get(), MODULE_INTERPRETER);
		registration.registerSubtypeInterpreter(ModItems.ITEM_MODULE_BATTERY.get(), MODULE_INTERPRETER);
	}
	
	@Override
	public void registerIngredients(IModIngredientRegistration registration) {
		assert Minecraft.getInstance().world != null;
		List<MMTIngredient> itemStacks = Minecraft.getInstance().world.getRecipeManager().getRecipes().stream().filter(r -> r instanceof MMTShapedRecipe).map(r -> (MMTShapedRecipe) r).map(MMTShapedRecipe::getIngredientsWithNBT).flatMap(Collection::stream).collect(Collectors.toList());
		registration.register(MMTIngredientType.MMT, itemStacks, INGREDIENT_HELPER, INGREDIENT_RENDERER);
	}
	
	@Override
	public void registerRecipes(IRecipeRegistration registration) {
		assert Minecraft.getInstance().world != null;
		List<MMTShapedRecipe> recipes = Minecraft.getInstance().world.getRecipeManager().getRecipes().stream().filter(r -> r instanceof MMTShapedRecipe).map(r -> (MMTShapedRecipe) r).collect(Collectors.toList());
		List<MMTIngredient> ingredients = Minecraft.getInstance().world.getRecipeManager().getRecipes().stream().filter(r -> r instanceof MMTShapedRecipe).map(r -> (MMTShapedRecipe) r).map(MMTShapedRecipe::getIngredientsWithNBT).flatMap(Collection::stream).distinct().collect(Collectors.toList());

		//registration.addIngredientInfo(ingredients, MMTIngredientType.MMT, "jei.modularmultitool.mmt.description");
		//registration.addRecipes(recipes, VanillaRecipeCategoryUid.CRAFTING);
	}
	
	@Override
	public void onRuntimeAvailable(IJeiRuntime jeiRuntime) {
		INGREDIENT_RENDERER.renderer = jeiRuntime.getIngredientManager().getIngredientRenderer(VanillaTypes.ITEM);
	}
}