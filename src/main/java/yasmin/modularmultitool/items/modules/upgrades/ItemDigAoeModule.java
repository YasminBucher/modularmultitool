package yasmin.modularmultitool.items.modules.upgrades;

import yasmin.modularmultitool.config.Settings;
import yasmin.modularmultitool.items.base.ItemUpgradeModuleBase;

public class ItemDigAoeModule extends ItemUpgradeModuleBase {
	public ItemDigAoeModule() {
		super();
	}
	
	@Override
	public boolean isEnabled() {
		return Settings.itemDigAoeModule;
	}
	
	@Override
	public int getMaxLevel() {
		return 0;
	}
}