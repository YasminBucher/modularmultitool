package yasmin.modularmultitool.energy;

import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import yasmin.modularmultitool.items.base.ItemEnergyModuleBase;

import javax.annotation.Nonnull;

public class ItemEnergyStorageProvider implements ICapabilityProvider {
	public LazyOptional<IEnergyStorage> energyStorage;
	
	public ItemEnergyStorageProvider(ItemStack stack, ItemEnergyModuleBase item) {
		this.energyStorage = LazyOptional.of(() -> new ItemEnergyStorage(stack, item));
	}

	@Nonnull
	@Override
	public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, Direction side) {
		if(cap == CapabilityEnergy.ENERGY) {
			return energyStorage.cast();
		}
		
		return LazyOptional.empty();
	}
}