package yasmin.modularmultitool.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.StringTextComponent;
import yasmin.modularmultitool.config.Settings;

public class CommandInfo {
	public static void register(CommandDispatcher<CommandSource> dispatch) {
		LiteralArgumentBuilder<CommandSource> builder = Commands.literal("mmt_info")
				.requires(src -> src.hasPermissionLevel(Settings.minPermLevelInfo))
				.requires(src -> src.getEntity() instanceof ServerPlayerEntity)
				.executes(CommandInfo::execute);
		
		dispatch.register(builder);
	}
	
	private static int execute(CommandContext<CommandSource> context) throws CommandSyntaxException {
		PlayerEntity player = context.getSource().asPlayer();
		ItemStack stack = player.getHeldItemMainhand();
		
		player.sendMessage(new StringTextComponent(String.format("name[%s]", stack.getItem().getRegistryName().toString())), null);
		player.sendMessage(new StringTextComponent(String.format("nbt[%s]", stack.getTag().toString())), null);
		
		return 1;
	}
}