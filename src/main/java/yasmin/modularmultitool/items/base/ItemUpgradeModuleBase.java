package yasmin.modularmultitool.items.base;

public class ItemUpgradeModuleBase extends ItemModuleBase {
	public ItemUpgradeModuleBase() {
		super();
	}
	
	@Override
	public int getMaxLevel() {
		return 5;
	}
}