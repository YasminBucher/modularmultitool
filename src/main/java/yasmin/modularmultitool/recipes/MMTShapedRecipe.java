package yasmin.modularmultitool.recipes;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.*;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.ICraftingRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

import javax.annotation.Nonnull;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class MMTShapedRecipe implements ICraftingRecipe, net.minecraftforge.common.crafting.IShapedRecipe<CraftingInventory> {
	public static final int MAX_HEIGHT = 3;
	public static final int MAX_WIDTH = 3;
	
	public final int recipeWidth;
	public final int recipeHeight;
	public final NonNullList<MMTIngredient> recipeItems;
	public final MMTCraftingResult craftingResult;
	public final ResourceLocation id;
	public final String group;
	
	public MMTShapedRecipe(ResourceLocation idIn, String groupIn, int recipeWidthIn, int recipeHeightIn, NonNullList<MMTIngredient> recipeItemsIn, MMTCraftingResult craftingResultIn) {
		this.id = idIn;
		this.group = groupIn;
		this.recipeWidth = recipeWidthIn;
		this.recipeHeight = recipeHeightIn;
		this.recipeItems = recipeItemsIn;
		this.craftingResult = craftingResultIn;
	}
	
	public static NonNullList<MMTIngredient> deserializeIngredients(String[] pattern, Map<String, MMTIngredient> keys, int patternWidth, int patternHeight) {
		NonNullList<MMTIngredient> nonnulllist = NonNullList.withSize(patternWidth * patternHeight, MMTIngredient.EMPTY);
		Set<String> set = Sets.newHashSet(keys.keySet());
		set.remove(" ");

		for(int i = 0; i < pattern.length; ++i) {
			for(int j = 0; j < pattern[i].length(); ++j) {
				String s = pattern[i].substring(j, j + 1);
				MMTIngredient ingredient = keys.get(s);
				if(ingredient == null) {
					throw new JsonSyntaxException("Pattern references symbol '" + s + "' but it's not defined in the key");
				}

				set.remove(s);
				nonnulllist.set(j + patternWidth * i, ingredient);
			}
		}

		if(!set.isEmpty()) {
			throw new JsonSyntaxException("Key defines symbols that aren't used in pattern: " + set);
		} else {
			return nonnulllist;
		}
	}
	
	public static String[] shrink(String... toShrink) {
		int i = Integer.MAX_VALUE;
		int j = 0;
		int k = 0;
		int l = 0;

		for(int i1 = 0; i1 < toShrink.length; ++i1) {
			String s = toShrink[i1];
			i = Math.min(i, firstNonSpace(s));
			int j1 = lastNonSpace(s);
			j = Math.max(j, j1);
			if(j1 < 0) {
				if(k == i1) {
					++k;
				}

				++l;
			} else {
				l = 0;
			}
		}

		if(toShrink.length == l) {
			return new String[0];
		} else {
			String[] astring = new String[toShrink.length - l - k];

			for(int k1 = 0; k1 < astring.length; ++k1) {
				astring[k1] = toShrink[k1 + k].substring(i, j + 1);
			}

			return astring;
		}
	}
	
	private static int firstNonSpace(String str) {
		int i;

		for(i = 0; i < str.length(); ++i) {
			if(str.charAt(i) != ' ')
				break;
		}

		return i;
	}
	
	private static int lastNonSpace(String str) {
		int i;

		for(i = str.length() - 1; i >= 0; --i) {
			if(str.charAt(i) != ' ')
				break;
		}

		return i;
	}
	
	public static String[] patternFromJson(JsonArray jsonArr) {
		String[] astring = new String[jsonArr.size()];
		if(astring.length > MAX_HEIGHT) {
			throw new JsonSyntaxException("Invalid pattern: too many rows, " + MAX_HEIGHT + " is maximum");
		} else if(astring.length == 0) {
			throw new JsonSyntaxException("Invalid pattern: empty pattern not allowed");
		} else {
			for(int i = 0; i < astring.length; ++i) {
				String s = JSONUtils.getString(jsonArr.get(i), "pattern[" + i + "]");
				if(s.length() > MAX_WIDTH) {
					throw new JsonSyntaxException("Invalid pattern: too many columns, " + MAX_WIDTH + " is maximum");
				}

				if(i > 0 && astring[0].length() != s.length()) {
					throw new JsonSyntaxException("Invalid pattern: each row must be the same width");
				}

				astring[i] = s;
			}

			return astring;
		}
	}
	
	public static Map<String, MMTIngredient> deserializeKey(JsonObject json) {
		Map<String, MMTIngredient> map = Maps.newHashMap();

		for(Map.Entry<String, JsonElement> entry : json.entrySet()) {
			if(entry.getKey().length() != 1) {
				throw new JsonSyntaxException("Invalid key entry: '" + entry.getKey() + "' is an invalid symbol (must be 1 character only).");
			}

			if(" ".equals(entry.getKey())) {
				throw new JsonSyntaxException("Invalid key entry: ' ' is a reserved symbol.");
			}

			map.put(entry.getKey(), MMTIngredient.deserialize(entry.getValue()));
		}

		map.put(" ", MMTIngredient.EMPTY);
		return map;
	}
	
	@SuppressWarnings("deprecation")
	public static MMTCraftingResult deserializeCraftingResult(JsonObject jsonObject) {
		String resultItemString = JSONUtils.getString(jsonObject, "item");

		Item item = Registry.ITEM.getOptional(new ResourceLocation(resultItemString)).orElseThrow(() -> new JsonSyntaxException("Unknown item '" + resultItemString + "'"));

		if(jsonObject.has("data")) {
			throw new JsonParseException("Disallowed data tag found");
		} else {
			int i = JSONUtils.getInt(jsonObject, "count", 1);
			ItemStack resultStack = new ItemStack(item, i);
			CompoundNBT resultNbt = null;

			try {
				resultNbt = JsonToNBT.getTagFromJson(JSONUtils.getString(jsonObject, "nbt"));
			} catch(Exception ignored) {

			}

			return new MMTCraftingResult(resultStack, resultNbt);
		}
	}
	
	@Override
	public boolean matches(@Nonnull CraftingInventory inv, @Nonnull World worldIn) {
		if(!this.getIngredientsWithNBT().isEmpty()) {
			for(int i = 0; i < this.getIngredientsWithNBT().size(); i++) {
				ItemStack stackInSlot = inv.getStackInSlot(i);

				if(!compareIngredientToStack(this.getIngredientsWithNBT().get(i), stackInSlot)) {
					return false;
				}
			}

			return true;
		}

		return false;
	}
	
	@Nonnull
	@Override
	public ItemStack getCraftingResult(@Nonnull CraftingInventory inv) {
		return this.getRecipeOutput().copy();
	}
	
	@Override
	public boolean canFit(int width, int height) {
		return width >= this.recipeWidth && height >= this.recipeHeight;
	}
	
	@Nonnull
	@Override
	public ItemStack getRecipeOutput() {
		return this.craftingResult.getStack();
	}
	
	@Nonnull
	@Override
	public NonNullList<ItemStack> getRemainingItems(CraftingInventory inv) {
		return NonNullList.withSize(inv.getSizeInventory(), ItemStack.EMPTY);
	}
	
	@Nonnull
	@Override
	public NonNullList<Ingredient> getIngredients() {
		NonNullList<Ingredient> ingredients = NonNullList.create();
		ingredients.addAll(getIngredientsWithNBT().stream().map(i -> (Ingredient) i).collect(Collectors.toList()));
		return ingredients;
	}
	
	@Nonnull
	@Override
	public ResourceLocation getId() {
		return this.id;
	}
	
	@Nonnull
	@Override
	public IRecipeSerializer<?> getSerializer() {
		return ModRecipes.SHAPED_WITH_NBT.get();
	}
	
	public NonNullList<MMTIngredient> getIngredientsWithNBT() {
		return this.recipeItems;
	}
	
	private boolean compareIngredientToStack(MMTIngredient ingredient, ItemStack itemStack) {
		if(ingredient.getAcceptedItem().getItem().getClass().equals(itemStack.getItem().getClass())) {
			if(ingredient.getIngredientNbt() != null) {
				if(itemStack.hasTag()) {
					for(String key : ingredient.getIngredientNbt().keySet()) {
						assert itemStack.getTag() != null;

						if(!itemStack.getTag().contains(key) || !Objects.equals(itemStack.getTag().get(key), ingredient.getIngredientNbt().get(key))) {
							return false;
						}
					}

					return true;
				} else {
					return false;
				}
			} else {
				return true;
			}
		}

		return false;
	}
	
	@Override
	public int getRecipeWidth() {
		return this.recipeWidth;
	}
	
	@Override
	public int getRecipeHeight() {
		return this.recipeHeight;
	}
}