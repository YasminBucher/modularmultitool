package yasmin.modularmultitool.items;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableMap;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.inventory.container.SimpleNamedContainerProvider;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.item.Rarity;
import net.minecraft.state.Property;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.RayTraceContext.BlockMode;
import net.minecraft.util.math.RayTraceContext.FluidMode;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.common.IForgeShearable;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.fml.RegistryObject;
import yasmin.modularmultitool.blocks.AutosmeltFakeBlockInfo;
import yasmin.modularmultitool.blocks.AutosmeltFakeBlockInfoProperty;
import yasmin.modularmultitool.blocks.ModBlocks;
import yasmin.modularmultitool.config.Settings;
import yasmin.modularmultitool.gui.MultiToolContainer;
import yasmin.modularmultitool.inventory.MultiToolInventory;
import yasmin.modularmultitool.items.base.*;
import yasmin.modularmultitool.items.modules.energy.ItemBatteryModule;
import yasmin.modularmultitool.items.modules.tools.ItemShearsModule;
import yasmin.modularmultitool.items.modules.tools.ItemShovelModule;
import yasmin.modularmultitool.items.modules.tools.ItemSwordModule;
import yasmin.modularmultitool.items.modules.upgrades.*;
import yasmin.modularmultitool.util.ItemUtil;
import yasmin.modularmultitool.util.TextComponentUtil;

import javax.annotation.Nonnull;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ItemMultiTool extends ItemModularToolBase {
	private static final ITextComponent NAME_COMPONENT = new TranslationTextComponent("modularmultitool");
	public static final double TRACE_DIST = 5.0D;
	
	public ItemMultiTool() {
		super();
	}
	
	@Override
	public boolean isEnabled() {
		return Settings.itemMultiTool;
	}
	
	@Nonnull
	@Override
	public ActionResultType onItemUse(ItemUseContext context) {
		if(java.util.Objects.requireNonNull(context.getPlayer()).isCrouching()) {
			return ActionResultType.PASS;
		}
		
		ItemStack stack = context.getPlayer().getHeldItem(context.getHand());
		
		ItemStack shovelModuleStack = ItemUtil.getInstalledModule(stack, ItemShovelModule.class);
		ItemStack batteryModuleStack = ItemUtil.getInstalledModule(stack, ItemBatteryModule.class);
		
		if(shovelModuleStack != null) {
			boolean canMakePath = false;
			
			if(batteryModuleStack != null) {
				
				if(ItemUtil.getCurrentEnergy(batteryModuleStack) >= Settings.shovelPathEnergyUsage) {
					ItemUtil.removeEnergy(batteryModuleStack, Settings.shovelPathEnergyUsage);
					canMakePath = true;
				}
			} else {
				if(ItemUtil.getDurability(shovelModuleStack) > 0) {
					ItemUtil.applyDamage(shovelModuleStack, 1);
					canMakePath = true;
				}
			}
			
			if(canMakePath) {
				return ModItems.ITEM_MODULE_SHOVEL.get().onItemUse(context);
			}
		}
		
		return super.onItemUse(context);
	}
	
	@Override
	public float getDestroySpeed(ItemStack stack, BlockState state) {
		if(this.canBlockBeBroken(stack, state)) {
			ItemStack toolStack = ItemUtil.getToolModuleForBlock(stack, state);
			
			if(toolStack != null) {
				return ItemUtil.getDestroySpeed(stack, toolStack, state);
			}
		}
		
		return 0.0F;
	}
	
	private boolean canBlockBeBroken(ItemStack stack, BlockState state) {
		ItemStack toolModuleStack = ItemUtil.getToolModuleForBlock(stack, state);
		
		if(toolModuleStack == null) {
			return false;
		}
		
		ItemStack batteryModuleStack = ItemUtil.getInstalledModule(stack, ItemBatteryModule.class);
		
		if(batteryModuleStack != null) {
			return ItemUtil.getCurrentEnergy(batteryModuleStack) >= this.getEnergyUsedToBreakBlock(stack);
		} else {
			return ItemUtil.getDurability(toolModuleStack) > 0;
		}
	}
	
	private int getEnergyUsedToBreakBlock(ItemStack stack) {
		//base energy used per block
		int energyUsed = Settings.baseEnergyUsage;
		
		//additional energy used for silk touch
		if(ItemUtil.getInstalledModule(stack, ItemSilktouchModule.class) != null) {
			energyUsed += Settings.silktouchEnergyUsage;
		}
		
		//additional energy used for autosmelt
		if(ItemUtil.getInstalledModule(stack, ItemAutosmeltModule.class) != null) {
			energyUsed += Settings.autosmeltEnergyUsage;
		}
		
		//additional energy used for fortune
		ItemStack stackFortuneUpgrade = ItemUtil.getInstalledModule(stack, ItemFortuneModule.class);
		
		if(stackFortuneUpgrade != null) {
			int fortuneLevel = ItemUtil.getLevel(stackFortuneUpgrade);
			energyUsed += Settings.fortuneEnergyUsage * (fortuneLevel + 1);
		}
		
		return energyUsed;
	}
	
	@Nonnull
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, @Nonnull Hand handIn) {
		if(handIn == Hand.MAIN_HAND) {
			if(!worldIn.isRemote && playerIn.isCrouching()) {
				INamedContainerProvider provider = this.getContainer();
				playerIn.openContainer(provider);
			}
		}
		
		return new ActionResult<>(ActionResultType.PASS, playerIn.getHeldItem(handIn));
	}
	
	@Override
	public boolean hitEntity(ItemStack stack, LivingEntity target, LivingEntity attacker) {
		float damage = Settings.baseDamage;
		
		ItemStack swordModuleStack = ItemUtil.getInstalledModule(stack, ItemSwordModule.class);
		
		if(swordModuleStack != null) {
			damage += Settings.weaponDamage;
			damage += ItemUtil.getLevel(swordModuleStack) * Settings.weaponDamagePerLevel;
		}
		
		ItemStack weaponDamageModuleStack = ItemUtil.getInstalledModule(stack, ItemWeaponDamageModule.class);
		
		if(weaponDamageModuleStack != null) {
			damage += ItemUtil.getLevel(weaponDamageModuleStack) * Settings.weaponDamageModuleAddonDamage;
		}
		
		return target.attackEntityFrom(DamageSource.causeMobDamage(attacker), damage);
	}
	
	@Override
	public boolean canHarvestBlock(BlockState blockIn) {
		return true;
	}
	
	@Override
	public ActionResultType itemInteractionForEntity(ItemStack stack, PlayerEntity playerIn, LivingEntity target, Hand hand) {
		ItemStack shearsModuleStack = ItemUtil.getInstalledModule(stack, ItemShearsModule.class);
		ItemStack batteryModuleStack = ItemUtil.getInstalledModule(stack, ItemBatteryModule.class);
		
		if(!target.world.isRemote && target instanceof IForgeShearable && shearsModuleStack != null) {
			boolean canShear = false;
			ItemShearsModule shearsModule = (ItemShearsModule) shearsModuleStack.getItem();
			
			if(batteryModuleStack != null) {
				if(ItemUtil.getCurrentEnergy(batteryModuleStack) >= this.getEnergyUsedToShearSheep(stack)) {
					ItemUtil.removeEnergy(batteryModuleStack, this.getEnergyUsedToShearSheep(stack));
					canShear = true;
				}
			} else {
				if(ItemUtil.getDurability(shearsModuleStack) > 0) {
					ItemUtil.applyDamage(shearsModuleStack, 1);
					canShear = true;
				}
			}
			
			if(canShear) {
				return shearsModule.itemInteractionForEntity(stack, playerIn, target, hand);
			}
		}
		
		return super.itemInteractionForEntity(stack, playerIn, target, hand);
	}
	
	private int getEnergyUsedToShearSheep(ItemStack stack) {
		int energyUsed = Settings.shearsEnergyUsage;
		
		//additional energy used for fortune
		ItemStack stackFortuneUpgrade = ItemUtil.getInstalledModule(stack, ItemFortuneModule.class);
		
		if(stackFortuneUpgrade != null) {
			int fortuneLevel = ItemUtil.getLevel(stackFortuneUpgrade);
			
			energyUsed += Settings.fortuneEnergyUsage * (fortuneLevel + 1);
		}
		
		return energyUsed;
	}
	
	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		for(ItemStack toolMod : ItemUtil.getInstalledModules(stack, ItemToolModuleBase.class)) {
			tooltip.add(TextComponentUtil.getModuleDisplayText(toolMod));
		}
		
		for(ItemStack toolMod : ItemUtil.getInstalledModules(stack, ItemUpgradeModuleBase.class)) {
			tooltip.add(TextComponentUtil.getModuleDisplayText(toolMod));
		}
		
		for(ItemStack toolMod : ItemUtil.getInstalledModules(stack, ItemEnergyModuleBase.class)) {
			tooltip.add(TextComponentUtil.getModuleDisplayText(toolMod));
		}
		
		for(ItemStack toolMod : ItemUtil.getInstalledModules(stack, ItemLaserModuleBase.class)) {
			tooltip.add(TextComponentUtil.getModuleDisplayText(toolMod));
		}
	}
	
	@Nonnull
	@Override
	public Rarity getRarity(ItemStack stack) {
		return Rarity.EPIC;
	}
	
	@Nonnull
	@Override
	public Set<ToolType> getToolTypes(ItemStack stack) {
		HashSet<ToolType> toolClasses = new HashSet<>();
		
		List<ItemStack> installedTools = ItemUtil.getInstalledModules(stack, ItemToolModuleBase.class);
		
		for(ItemStack toolStack : installedTools) {
			toolClasses.add(((ItemToolModuleBase) toolStack.getItem()).toolType);
		}
		
		return toolClasses;
	}
	
	public INamedContainerProvider getContainer() {
		return new SimpleNamedContainerProvider((id, playerInv, player) -> new MultiToolContainer(id, playerInv, new MultiToolInventory(player.getHeldItemMainhand())), NAME_COMPONENT);
	}
	
	@Override
	public boolean onBlockStartBreak(ItemStack itemstack, BlockPos pos, PlayerEntity player) {
		boolean result = false;
		ItemStack fortuneModuleStack = ItemUtil.getInstalledModule(itemstack, ItemFortuneModule.class);
		
		if(!player.world.isRemote) {
			if(ItemUtil.getInstalledModule(itemstack, ItemSilktouchModule.class) != null) {
				ItemUtil.addEnchantment(itemstack, Enchantments.SILK_TOUCH, 1);
			} else if(fortuneModuleStack != null) {
				ItemUtil.addEnchantment(itemstack, Enchantments.FORTUNE, ItemUtil.getLevel(fortuneModuleStack));
			}
			
			int aoeRadius = 0;
			
			if(ItemUtil.getInstalledModule(itemstack, ItemDigAoeModule.class) != null) {
				aoeRadius = 1;
			}
			
			result = this.breakBlocks(itemstack, pos, player, aoeRadius);
		}
		
		ItemUtil.removeEnchantment(itemstack, Enchantments.SILK_TOUCH);
		ItemUtil.removeEnchantment(itemstack, Enchantments.FORTUNE);
		
		return result;
	}
	
	private boolean breakBlocks(ItemStack stack, BlockPos originalBlockPos, PlayerEntity player, int aoeRadius) {
		if(!this.breakBlock(stack, originalBlockPos, player)) {
			return false;
		}
		
		if(aoeRadius > 0) {
			BlockRayTraceResult ray = this.playerRayTraceBlock(player);
			Direction side = ray.getFace();
			BlockPos start = originalBlockPos;
			BlockPos end = originalBlockPos;
			
			if(side == Direction.DOWN || side == Direction.UP) {
				start = originalBlockPos.add(-aoeRadius, 0, -aoeRadius);
				end = originalBlockPos.add(aoeRadius, 0, aoeRadius);
			} else if(side == Direction.NORTH || side == Direction.SOUTH) {
				start = originalBlockPos.add(-aoeRadius, -aoeRadius, 0);
				end = originalBlockPos.add(aoeRadius, aoeRadius, 0);
			} else if(side == Direction.EAST || side == Direction.WEST) {
				start = originalBlockPos.add(0, -aoeRadius, -aoeRadius);
				end = originalBlockPos.add(0, aoeRadius, aoeRadius);
			}
			
			for(int x = start.getX(); x <= end.getX(); x++) {
				for(int y = start.getY(); y <= end.getY(); y++) {
					for(int z = start.getZ(); z <= end.getZ(); z++) {
						if(!(x == originalBlockPos.getX() && y == originalBlockPos.getY() && z == originalBlockPos.getZ())) {
							BlockPos currentBlockPos = new BlockPos(x, y, z);
							BlockState state = player.world.getBlockState(currentBlockPos);
							
							if(!Objects.equal(state.getBlock(), Blocks.AIR)) {
								this.breakBlock(stack, currentBlockPos, player);
							}
						}
					}
				}
			}
		}
		
		return true;
	}
	
	private boolean breakBlock(ItemStack stack, BlockPos pos, PlayerEntity player) {
		BlockState state = player.world.getBlockState(pos);
		Block block = state.getBlock();
		
		if(!this.canBlockBeBroken(stack, state)) {
			return false;
		}
		
		block.removedByPlayer(state, player.world, pos, player, true, state.getFluidState());
		block.onBlockHarvested(player.world, pos, state, player);
		
		ItemStack autoSmeltModule = ItemUtil.getInstalledModule(stack, ItemAutosmeltModule.class);
		
		if(autoSmeltModule != null && ItemUtil.getBlockHasSmeltingResult(state, player.world, pos)) {
			Block blockNew = ModBlocks.AUTOSMELTFAKE.get();
			BlockState newState = blockNew.getDefaultState().with(AutosmeltFakeBlockInfoProperty.AFBI_PROP, new AutosmeltFakeBlockInfo(state, stack));
			
			blockNew.harvestBlock(player.world, player, pos, newState, null, stack);
		} else {
			block.harvestBlock(player.world, player, pos, state, null, stack);
		}
		
		ItemStack batteryModuleStack = ItemUtil.getInstalledModule(stack, ItemBatteryModule.class);
		
		if(batteryModuleStack != null) {
			ItemUtil.removeEnergy(batteryModuleStack, this.getEnergyUsedToBreakBlock(stack));
		} else {
			ItemStack toolModuleStack = ItemUtil.getToolModuleForBlock(stack, state);
			
			if(toolModuleStack != null) {
				ItemUtil.applyDamage(toolModuleStack, 1);
			}
		}
		
		return true;
	}
	
	private BlockRayTraceResult playerRayTraceBlock(PlayerEntity player) {
		double dist = TRACE_DIST;
		
		if(player instanceof ServerPlayerEntity && Minecraft.getInstance().playerController != null) {
			dist = Minecraft.getInstance().playerController.getBlockReachDistance();
		}
		
		Vector3d plv = Vector3d.fromPitchYaw(player.getPitchYaw()).scale(dist);
		Vector3d vecStart = new Vector3d(player.getPosition().getX(), player.getPosition().getY() + (double) player.getEyeHeight(), player.getPosition().getZ());
		
		Vector3d vecEnd = vecStart.add(plv);
		
		RayTraceContext context = new RayTraceContext(vecStart, vecEnd, BlockMode.COLLIDER, FluidMode.ANY, player);
		return player.world.rayTraceBlocks(context);
	}
	
	@Override
	public boolean onEntitySwing(ItemStack stack, LivingEntity entity) {
		return true;
	}
	
	@Override
	public int getDamage(ItemStack stack) {
		return 0;
	}
	
	@Override
	public boolean showDurabilityBar(ItemStack stack) {
		return ItemUtil.getInstalledModule(stack, ItemBatteryModule.class) != null && this.getDurabilityForDisplay(stack) > 0.0d;
	}
	
	@Override
	public double getDurabilityForDisplay(ItemStack stack) {
		RegistryObject<Item> batteryRegObject = ModItems.ITEM_MODULE_BATTERY;
		
		if(batteryRegObject.isPresent()) {
			ItemStack batteryStack = ItemUtil.getInstalledModule(stack, ItemBatteryModule.class);
			
			if(batteryStack != null) {
				return 1.0d - ItemUtil.getEnergyPercentange(batteryStack);
			}
		}
		
		return 0.0d;
	}
	
	@Override
	public boolean canHarvestBlock(ItemStack stack, BlockState state) {
		ItemStack toolModuleStack = ItemUtil.getToolModuleForBlock(stack, state);
		
		if(toolModuleStack != null) {
			return ItemUtil.canHarvestBlock(stack, toolModuleStack, state);
		}
		
		return false;
	}
	
	@Override
	public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged) {
		return slotChanged;
	}
}