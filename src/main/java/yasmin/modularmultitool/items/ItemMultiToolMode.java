package yasmin.modularmultitool.items;

import java.util.HashMap;
import java.util.Map;

public enum ItemMultiToolMode {
	NORMAL(0),
	LASER(1);
	
	private static Map<Integer, ItemMultiToolMode> map = new HashMap<>();
	
	static {
		for(ItemMultiToolMode mode : ItemMultiToolMode.values()) {
			map.put(mode.value, mode);
		}
	}
	
	private int value;
	
	ItemMultiToolMode(int i) {
		this.value = i;
	}
	
	public static ItemMultiToolMode valueOf(int value) {
		return map.get(value);
	}
	
	public int getValue() {
		return this.value;
	}
}