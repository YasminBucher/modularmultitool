package yasmin.modularmultitool.gui;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.energy.CapabilityEnergy;
import yasmin.modularmultitool.items.base.ItemModuleBase;

public class MultiToolMachineOutputSlot extends Slot {
	public MultiToolMachineOutputSlot(IInventory inventoryIn, int index, int xPosition, int yPosition) {
		super(inventoryIn, index, xPosition, yPosition);
	}
	
	@Override
	public boolean isItemValid(ItemStack stack) {
		assert stack != null;
		
		return !stack.isEmpty() && (stack.getItem() instanceof ItemModuleBase || stack.getCapability(CapabilityEnergy.ENERGY).isPresent());
	}
	
	@Override
	public int getSlotStackLimit() {
		return 1;
	}
}