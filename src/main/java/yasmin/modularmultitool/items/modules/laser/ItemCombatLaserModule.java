package yasmin.modularmultitool.items.modules.laser;

import yasmin.modularmultitool.config.Settings;
import yasmin.modularmultitool.items.base.ItemLaserModuleBase;

public class ItemCombatLaserModule extends ItemLaserModuleBase {
	public ItemCombatLaserModule() {
		super();
	}
	
	@Override
	public boolean isEnabled() {
		return Settings.itemCombatLaserModule;
	}
}