package yasmin.modularmultitool.machines;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.common.util.Constants;
import yasmin.modularmultitool.gui.MultiToolMachineContainer;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Arrays;

public class MultiToolMachineTileEntity extends TileEntity implements INamedContainerProvider, IInventory {
	public static final int INV_SIZE = 10;
	private static final ITextComponent DISPLAY_NAME = new StringTextComponent("MultiTool Machine");
	
	private ItemStack[] inventory = new ItemStack[INV_SIZE];
	
	public MultiToolMachineTileEntity() {
		super(ModTileEntityTypes.MULTITOOLMACHINE.get());
	}
	
	@Override
	public void read(BlockState state, CompoundNBT compound) {
		ListNBT items = compound.getList("MultiToolMachineItems", Constants.NBT.TAG_COMPOUND);
		
		if(!items.isEmpty()) {
			for(int i = 0; i < items.size(); i++) {
				CompoundNBT item = items.getCompound(i);
				int slot = item.getInt("InventorySlot");
				
				if(slot >= 0 && slot < this.getSizeInventory()) {
					ItemStack itemStack = ItemStack.read(item);
					
					if(this.isItemValidForSlot(slot, itemStack)) {
						this.inventory[slot] = itemStack;
					}
				}
			}
		}
	}
	
	@Nonnull
	@Override
	public CompoundNBT write(CompoundNBT compound) {
		ListNBT items = new ListNBT();
		
		for(int i = 0; i < this.getSizeInventory(); i++) {
			if(!this.getStackInSlot(i).isEmpty()) {
				CompoundNBT item = new CompoundNBT();
				item.putInt("InventorySlot", i);
				this.getStackInSlot(i).write(item);
				
				items.add(item);
			}
		}
		
		compound.put("MultiToolMachineItems", items);
		
		return compound;
	}
	
	@Nonnull
	@Override
	public ITextComponent getDisplayName() {
		return DISPLAY_NAME;
	}
	
	@Nullable
	@Override
	public Container createMenu(int i, @Nonnull PlayerInventory playerInv, @Nonnull PlayerEntity player) {
		return new MultiToolMachineContainer(i, playerInv, this);
	}
	
	@Override
	public int getSizeInventory() {
		return this.inventory.length;
	}
	
	@Override
	public boolean isEmpty() {
		return Arrays.stream(this.inventory).allMatch(ItemStack::isEmpty);
	}
	
	@Nonnull
	@Override
	public ItemStack getStackInSlot(int index) {
		if(this.inventory[index] == null) {
			this.inventory[index] = ItemStack.EMPTY;
		}
		
		return this.inventory[index];
	}
	
	@Nonnull
	@Override
	public ItemStack decrStackSize(int index, int count) {
		ItemStack stack = this.getStackInSlot(index);
		
		if(!stack.isEmpty()) {
			if(stack.getCount() > count) {
				stack = stack.split(count);
				this.markDirty();
			} else {
				this.setInventorySlotContents(index, ItemStack.EMPTY);
			}
		}
		
		return stack;
	}
	
	@Nonnull
	@Override
	public ItemStack removeStackFromSlot(int index) {
		ItemStack stack = this.getStackInSlot(index);
		
		if(!stack.isEmpty()) {
			this.setInventorySlotContents(index, ItemStack.EMPTY);
		}
		
		return stack;
	}
	
	@Override
	public void setInventorySlotContents(int index, @Nonnull ItemStack stack) {
		this.inventory[index] = stack;
		
		if(!stack.isEmpty() && stack.getCount() > this.getInventoryStackLimit()) {
			stack.setCount(this.getInventoryStackLimit());
		}
	}
	
	@Override
	public boolean isUsableByPlayer(@Nonnull PlayerEntity player) {
		return true;
	}
	
	@Override
	public void clear() {
		for(int i = 0; i < this.getSizeInventory(); i++) {
			this.inventory[i] = ItemStack.EMPTY;
		}
	}
	
	@Override
	public void markDirty() {
		this.write(this.getTileData());
	}
}