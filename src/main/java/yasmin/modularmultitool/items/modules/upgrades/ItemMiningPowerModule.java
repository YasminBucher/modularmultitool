package yasmin.modularmultitool.items.modules.upgrades;

import yasmin.modularmultitool.config.Settings;
import yasmin.modularmultitool.items.base.ItemUpgradeModuleBase;

public class ItemMiningPowerModule extends ItemUpgradeModuleBase {
	public ItemMiningPowerModule() {
		super();
	}
	
	@Override
	public boolean isEnabled() {
		return Settings.itemMiningPowerModule;
	}
}