package yasmin.modularmultitool.items.modules.upgrades;

import yasmin.modularmultitool.config.Settings;
import yasmin.modularmultitool.items.base.ItemUpgradeModuleBase;

public class ItemDigSpeedModule extends ItemUpgradeModuleBase {
	public ItemDigSpeedModule() {
		super();
	}
	
	@Override
	public boolean isEnabled() {
		return Settings.itemDigSpeedModule;
	}
}