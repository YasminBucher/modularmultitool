package yasmin.modularmultitool.util;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ToolItem;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import org.apache.logging.log4j.Level;
import yasmin.modularmultitool.ModularMultiTool;

import java.util.Collections;
import java.util.Set;

public class ReflectionUtil {
	public static Set<Block> getEffectiveOn(Item item) {
		if(item instanceof ToolItem) {
			Class<ToolItem> clazz = ToolItem.class;
			
			try {
				return ObfuscationReflectionHelper.getPrivateValue(clazz, (ToolItem) item, "effectiveBlocks");
			} catch(Exception e) {
				ModularMultiTool.LOGGER.log(Level.ERROR, e.getMessage());
			}
		}
		
		return Collections.emptySet();
	}
}