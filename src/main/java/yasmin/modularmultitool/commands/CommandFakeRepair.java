package yasmin.modularmultitool.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import yasmin.modularmultitool.config.Settings;
import yasmin.modularmultitool.items.base.ItemToolModuleBase;
import yasmin.modularmultitool.util.ItemUtil;

public class CommandFakeRepair {
	public static void register(CommandDispatcher<CommandSource> dispatch) {
		LiteralArgumentBuilder<CommandSource> builder = Commands.literal("mmt_repair")
				.requires(src -> src.hasPermissionLevel(Settings.minPermLevelRepair))
				.requires(src -> src.getEntity() instanceof ServerPlayerEntity)
				.then(Commands.argument("amount", IntegerArgumentType.integer())
						.executes(CommandFakeRepair::executeWithArguments))
				.executes(CommandFakeRepair::executeWithoutArguments);
		
		dispatch.register(builder);
	}
	
	private static int executeWithArguments(CommandContext<CommandSource> context) throws CommandSyntaxException {
		return execute(context, context.getArgument("amount", Integer.class));
	}
	
	private static int executeWithoutArguments(CommandContext<CommandSource> context) throws CommandSyntaxException {
		return execute(context, 0);
	}
	
	private static int execute(CommandContext<CommandSource> context, int amount) throws CommandSyntaxException {
		ServerPlayerEntity player = context.getSource().asPlayer();
		ItemStack stack = player.getHeldItemMainhand();
		
		if(stack.getItem() instanceof ItemToolModuleBase) {
			if(amount == 0) {
				amount = ItemUtil.getMaxDurability(stack);
			}
			
			ItemUtil.setDurability(stack, amount);
		}
		
		return 1;
	}
}