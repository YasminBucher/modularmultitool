package yasmin.modularmultitool.items.base;

public class ItemLaserModuleBase extends ItemModuleBase {
	public ItemLaserModuleBase() {
		super();
	}
	
	@Override
	public int getMaxLevel() {
		return 5;
	}
}