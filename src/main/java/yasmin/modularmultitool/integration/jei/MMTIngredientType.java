package yasmin.modularmultitool.integration.jei;

import mezz.jei.api.ingredients.IIngredientType;
import yasmin.modularmultitool.recipes.MMTIngredient;

import javax.annotation.Nonnull;

public class MMTIngredientType implements IIngredientType<MMTIngredient> {
	public static final MMTIngredientType MMT = new MMTIngredientType();
	
	@Nonnull
	@Override
	public Class<? extends MMTIngredient> getIngredientClass() {
		return MMTIngredient.class;
	}
}
