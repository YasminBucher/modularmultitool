package yasmin.modularmultitool.gui;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import yasmin.modularmultitool.ModularMultiTool;

public class MultiToolContainerScreen extends ContainerScreen<MultiToolContainer> {
	private final static ResourceLocation MULTITOOL_INVENTORY_TEXTURE = new ResourceLocation(ModularMultiTool.MODID, "textures/gui/gui_multitool.png");
	private final static ResourceLocation MULTITOOL_PLAYERINVENTORY_TEXTURE = new ResourceLocation(ModularMultiTool.MODID, "textures/gui/gui_player_inv.png");
	
	public static final int TEXTCOLOR = 0x222222;
	public static final float HALF = 2F;
	public static final int X_SIZE = 174;
	public static final int Y_SIZE = 275;
	public static final int TITLE_YPOS = -20;
	public static final int INVENTORY_HEIGHT = 166;
	public static final int PLAYER_INVENTORY_HEIGHT = 89;
	public static final int INV_TO_PINV_SPACER = 20;
	
	public MultiToolContainerScreen(MultiToolContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
		super(screenContainer, inv, titleIn);
		
		this.xSize = X_SIZE;
		this.ySize = Y_SIZE;
	}
	
	@Override
	public void render(MatrixStack mstack, int p_render_1_, int p_render_2_, float p_render_3_) {
		this.renderBackground(mstack);
		super.render(mstack, p_render_1_, p_render_2_, p_render_3_);
		this.renderHoveredTooltip(mstack, p_render_1_, p_render_2_);
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(MatrixStack mstack, int mouseX, int mouseY) {
		String text = "Modular MultiTool";
		this.font.drawString(mstack, text, this.xSize / HALF - this.font.getStringWidth(text) / HALF, TITLE_YPOS, TEXTCOLOR);
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(MatrixStack mstack, float partialTicks, int mouseX, int mouseY) {
		RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
		
		assert this.minecraft != null;
		this.minecraft.getTextureManager().bindTexture(MULTITOOL_INVENTORY_TEXTURE);
		this.blit(mstack, this.guiLeft, this.guiTop, 0, 0, X_SIZE, INVENTORY_HEIGHT);
		
		this.minecraft.getTextureManager().bindTexture(MULTITOOL_PLAYERINVENTORY_TEXTURE);
		this.blit(mstack, this.guiLeft, this.guiTop + INVENTORY_HEIGHT + INV_TO_PINV_SPACER, 0, 0, X_SIZE, PLAYER_INVENTORY_HEIGHT);
	}
}