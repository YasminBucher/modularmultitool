package yasmin.modularmultitool.items.modules.upgrades;

import yasmin.modularmultitool.config.Settings;
import yasmin.modularmultitool.items.base.ItemUpgradeModuleBase;

public class ItemAutosmeltModule extends ItemUpgradeModuleBase {
	public ItemAutosmeltModule() {
		super();
	}
	
	@Override
	public boolean isEnabled() {
		return Settings.itemAutosmeltModule;
	}
	
	@Override
	public int getMaxLevel() {
		return 0;
	}
}