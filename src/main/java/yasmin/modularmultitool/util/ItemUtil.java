package yasmin.modularmultitool.util;

import com.google.common.base.Objects;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.material.Material;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.FurnaceRecipe;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import yasmin.modularmultitool.inventory.MultiToolInventory;
import yasmin.modularmultitool.items.base.ItemModuleBase;
import yasmin.modularmultitool.items.base.ItemToolModuleBase;
import yasmin.modularmultitool.items.modules.tools.ItemShearsModule;
import yasmin.modularmultitool.items.modules.tools.ItemSwordModule;
import yasmin.modularmultitool.items.modules.upgrades.ItemDigSpeedModule;
import yasmin.modularmultitool.items.modules.upgrades.ItemMiningPowerModule;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

public class ItemUtil {
	public static final int MAX_DURABILITY_LV1 = 64;
	public static final int MAX_ENERGY_LV0 = 10000;
	
	public static void addEnchantment(ItemStack stack, Enchantment e, int level) {
		if(EnchantmentHelper.getEnchantmentLevel(e, stack) == 0) {
			stack.addEnchantment(e, level);
		}
	}
	
	public static void removeEnchantment(ItemStack stack, Enchantment e) {
		ListNBT ench = stack.getEnchantmentTagList();
		
		int counter = 0;
		int size = ench.size();
		
		while(counter < size) {
			CompoundNBT cnbt = ench.getCompound(counter);
			String enchId = cnbt.getString("id");
			
			if(enchId.equalsIgnoreCase(java.util.Objects.requireNonNull(e.getRegistryName()).toString())) {
				ench.remove(counter);
				size--;
			} else {
				counter++;
			}
		}
		
		if(ench.size() == 0 && stack.getTag() != null) {
			stack.getTag().remove("ench");
		}
	}
	
	public static int getMaxDurability(ItemStack stack) {
		int level = getLevel(stack);
		return (int) Math.pow(2, level) * MAX_DURABILITY_LV1;
	}
	
	public static int getDurability(ItemStack stack) {
		CompoundNBT cnbt = stack.getTag();
		
		if(cnbt == null) {
			cnbt = new CompoundNBT();
			stack.setTag(cnbt);
		}
		
		if(!cnbt.contains("mmt_durability")) {
			cnbt.putInt("mmt_durability", getMaxDurability(stack));
		}
		
		return cnbt.getInt("mmt_durability");
	}
	
	public static void setDurability(ItemStack stack, int durability) {
		int capBottom = 0;
		int capTop = getMaxDurability(stack);
		
		durability = Math.max(capBottom, durability);
		durability = Math.min(capTop, durability);
		
		CompoundNBT cnbt = stack.getTag();
		
		if(cnbt == null) {
			cnbt = new CompoundNBT();
			stack.setTag(cnbt);
		}
		
		cnbt.putInt("mmt_durability", durability);
	}
	
	public static void applyDamage(ItemStack stack, int damage) {
		int currentDurability = getDurability(stack);
		int newDurability = Math.max(0, currentDurability - damage);
		setDurability(stack, newDurability);
	}
	
	public static double getDurabilityPercentage(ItemStack stack) {
		return Math.min((double) getDurability(stack) / (double) getMaxDurability(stack), 1.0d);
	}
	
	public static void setLevel(ItemStack stack, int level) {
		CompoundNBT cnbt = stack.getTag();
		
		if(cnbt == null) {
			cnbt = new CompoundNBT();
			stack.setTag(cnbt);
		}
		
		cnbt.putInt("mmt_level", level);
	}
	
	public static int getMaxEnergy(int level) {
		return (int) Math.pow(2, level) * MAX_ENERGY_LV0;
	}
	
	public static void addEnergy(ItemStack stack, int energy) {
		IEnergyStorage energyStorage = getEnergyStorage(stack);
		
		if(energyStorage != null) {
			energyStorage.receiveEnergy(energy, false);
		}
	}
	
	public static IEnergyStorage getEnergyStorage(ItemStack stack) {
		LazyOptional<IEnergyStorage> lazyStorage = stack.getCapability(CapabilityEnergy.ENERGY);
		AtomicReference<IEnergyStorage> iEnergyStorage = new AtomicReference<>();
		lazyStorage.ifPresent(iEnergyStorage::set);
		
		return iEnergyStorage.get();
	}
	
	public static void removeEnergy(ItemStack stack, int energy) {
		IEnergyStorage energyStorage = getEnergyStorage(stack);
		
		if(energyStorage != null) {
			energyStorage.extractEnergy(energy, false);
		}
	}
	
	public static double getEnergyPercentange(ItemStack stack) {
		return Math.min((double) ItemUtil.getCurrentEnergy(stack) / (double) ItemUtil.getMaxEnergy(stack), 1.0d);
	}
	
	public static int getCurrentEnergy(ItemStack stack) {
		IEnergyStorage energyStorage = getEnergyStorage(stack);
		
		if(energyStorage != null) {
			return energyStorage.getEnergyStored();
		}
		
		return 0;
	}
	
	public static int getMaxEnergy(ItemStack stack) {
		IEnergyStorage energyStorage = getEnergyStorage(stack);
		
		if(energyStorage != null) {
			return energyStorage.getMaxEnergyStored();
		} else {
			return 0;
		}
	}
	
	public static Item getUpgradeItemForLevel(int level) {
		switch(level) {
			case 1:
				return Items.STONE;
			case 2:
				return Items.IRON_INGOT;
			case 3:
				return Items.GOLD_INGOT;
			case 4:
				return Items.DIAMOND;
			case 5:
				return Items.EMERALD;
			default:
				return Items.AIR;
		}
	}
	
	public static boolean getBlockHasSmeltingResult(BlockState blockStateIn, World worldIn, BlockPos blockPosIn) {
		List<ItemStack> drops = Block.getDrops(blockStateIn, (ServerWorld) worldIn, blockPosIn, null);
		boolean hasSmeltingResult = false;
		
		for(ItemStack drop : drops) {
			Optional<FurnaceRecipe> optionalFurnaceRecipe = worldIn.getRecipeManager().getRecipe(IRecipeType.SMELTING, new Inventory(drop), worldIn);
			
			if(optionalFurnaceRecipe.isPresent()) {
				hasSmeltingResult = true;
				break;
			}
		}
		
		return hasSmeltingResult;
	}
	
	public static ItemStack getToolModuleForBlock(ItemStack stack, BlockState state) {
		for(ItemStack modStack : getInstalledModules(stack, ItemToolModuleBase.class)) {
			if(ItemUtil.getDestroySpeed(stack, modStack, state) > 0.0F) {
				return modStack;
			}
		}
		
		return null;
	}
	
	public static List<ItemStack> getInstalledModules(ItemStack stack, Class<? extends ItemModuleBase> modClass) {
		List<ItemStack> installedModules = new ArrayList<>();
		
		MultiToolInventory inventory = new MultiToolInventory(stack);
		
		for(int i = 0; i < inventory.getSizeInventory(); i++) {
			ItemStack stackModule = inventory.getStackInSlot(i);
			
			if(modClass.isAssignableFrom(stackModule.getItem().getClass())) {
				installedModules.add(stackModule);
			}
		}
		
		return installedModules;
	}
	
	public static float getDestroySpeed(ItemStack multitoolStack, ItemStack toolModuleStack, BlockState state) {
		if(toolModuleStack.getItem() instanceof ItemToolModuleBase) {
			ItemToolModuleBase itemToolModule = (ItemToolModuleBase) toolModuleStack.getItem();
			
			Block block = state.getBlock();
			float toolLevel = getLevel(toolModuleStack);
			float destroySpeed = 0.0F;
			float multiplier = 1.0F;
			
			if(itemToolModule.toolType == ItemSwordModule.TT_SWORD) {
				if(block == Blocks.COBWEB) {
					destroySpeed = 15.0F;
				} else {
					Material material = state.getMaterial();
					
					if(material == Material.PLANTS || material == Material.TALL_PLANTS || material == Material.CORAL || material == Material.GOURD) {
						destroySpeed = 1.5F;
					} else if(state.isIn(BlockTags.LEAVES)) {
						destroySpeed = 1.5F;
					} else {
						destroySpeed = 1.0F;
					}
				}
			} else if(itemToolModule.toolType == ItemShearsModule.TT_SHEARS) {
				if(block == Blocks.COBWEB || state.isIn(BlockTags.LEAVES)) {
					destroySpeed = 15.0F;
				} else if(block.isIn(BlockTags.WOOL)) {
					destroySpeed = 5.0F;
				}
			} else {
				if(state.isToolEffective(itemToolModule.getToolType())) {
					destroySpeed = itemToolModule.strVsProperBlocks;
				} else {
					destroySpeed = 1.0F;
				}
				
				multiplier = getSpeedModifier(multitoolStack);
			}
			
			return destroySpeed + multiplier + toolLevel;
		}
		
		return 0.0F;
	}
	
	public static int getLevel(ItemStack stack) {
		CompoundNBT cnbt = stack.getTag();
		
		if(cnbt != null && cnbt.contains("mmt_level")) {
			return cnbt.getInt("mmt_level");
		}
		
		return 0;
	}
	
	public static float getSpeedModifier(ItemStack multitoolStack) {
		int level = 0;
		
		ItemStack digSpeedStack = getInstalledModule(multitoolStack, ItemDigSpeedModule.class);
		
		if(digSpeedStack != null) {
			ItemUtil.getLevel(digSpeedStack);
		}
		
		return (float) (level * 3);
	}
	
	public static ItemStack getInstalledModule(ItemStack stack, Class<? extends ItemModuleBase> itemModuleClass) {
		MultiToolInventory inventory = new MultiToolInventory(stack);
		
		for(int i = 0; i < inventory.getSizeInventory(); i++) {
			ItemStack stackModule = inventory.getStackInSlot(i);
			
			if(Objects.equal(stackModule.getItem().getClass(), itemModuleClass)) {
				return stackModule;
			}
		}
		
		return null;
	}
	
	public static boolean canHarvestBlock(ItemStack multitoolStack, ItemStack toolModuleStack, BlockState state) {
		if(toolModuleStack.getItem() instanceof ItemToolModuleBase) {
			Block block = state.getBlock();
			ItemToolModuleBase toolModule = (ItemToolModuleBase) toolModuleStack.getItem();
			
			if(toolModule.toolType == ItemSwordModule.TT_SWORD) {
				return block == Blocks.COBWEB;
			} else if(toolModule.toolType == ItemShearsModule.TT_SHEARS) {
				return block == Blocks.COBWEB || block == Blocks.REDSTONE_WIRE || block == Blocks.TRIPWIRE;
			} else {
				boolean isCorrectToolType = toolModule.toolType == state.getHarvestTool();
				int toolLevel = getLevel(toolModuleStack);
				int miningPowerLevel = 0;
				ItemStack miningPowerModuleStack = getInstalledModule(multitoolStack, ItemMiningPowerModule.class);
				
				if(miningPowerModuleStack != null) {
					miningPowerLevel = getLevel(miningPowerModuleStack) + 1;
				}
				
				return isCorrectToolType && (toolLevel + miningPowerLevel) >= state.getHarvestLevel();
			}
		}
		
		return false;
	}
}