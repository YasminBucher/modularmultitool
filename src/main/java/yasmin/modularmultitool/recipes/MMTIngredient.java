package yasmin.modularmultitool.recipes;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.stream.Stream;

public class MMTIngredient extends Ingredient {
	public static final MMTIngredient EMPTY = new MMTIngredient(ItemStack.EMPTY);
	private final ItemStack acceptedItem;
	private CompoundNBT ingredientNbt;
	
	public MMTIngredient(ItemStack stack) {
		super(Stream.empty());
		this.acceptedItem = stack;
	}
	
	public static MMTIngredient fromItem(IItemProvider itemIn) {
		return fromStack(new ItemStack(itemIn));
	}
	
	public static MMTIngredient fromStack(ItemStack stack) {
		return new MMTIngredient(stack);
	}
	
	public static MMTIngredient deserialize(@Nullable JsonElement json) {
		if(json != null && !json.isJsonNull()) {

			if(json.isJsonObject()) {
				JsonObject jsonObject = json.getAsJsonObject();

				if(jsonObject.has("item")) {
					Item item = Registry.ITEM.getOrDefault(new ResourceLocation(JSONUtils.getString(jsonObject, "item")));

					if(item != Items.AIR) {
						ItemStack itemStack = new ItemStack(item);

						if(jsonObject.has("nbt")) {
							try {
								itemStack.setTag(JsonToNBT.getTagFromJson(JSONUtils.getString(jsonObject, "nbt")));
							} catch(Exception ex) {
								throw new JsonSyntaxException("Invalid NBT data in recipe for item " + item);
							}
						}

						return new MMTIngredient(itemStack);
					} else {
						throw new JsonSyntaxException("Expected 'item' to be a valid item");
					}
				} else {
					throw new JsonSyntaxException("Expected member 'item' not found");
				}
			} else {
				throw new JsonSyntaxException("Expected json to be object");
			}
		} else {
			throw new JsonSyntaxException("json cannot be null");
		}
	}
	
	@Nonnull
	public static MMTIngredient read(PacketBuffer buffer) {
		int i = buffer.readVarInt();
		if(i == -1) {
			MMTIngredientSerializer serializer = MMTIngredientSerializer.INSTANCE;
			return serializer.parse(buffer);
		}

		return fromStack(buffer.readItemStack());
	}
	
	public MMTIngredient withNbt(CompoundNBT compoundNBT) {
		this.ingredientNbt = compoundNBT;
		return this;
	}
	
	public CompoundNBT getIngredientNbt() {
		return this.ingredientNbt;
	}
	
	public ItemStack getAcceptedItem() {
		return this.acceptedItem;
	}
	
	@Override
	public boolean test(ItemStack itemStack) {
		if(this.acceptedItem.getItem() == itemStack.getItem()) {
			if(this.ingredientNbt != null) {
				if(itemStack.getTag() != null) {
					CompoundNBT stackCompoundNBT = itemStack.getTag();

					for(String nbtId : this.ingredientNbt.keySet()) {
						INBT ingInbt = this.ingredientNbt.get(nbtId);
						INBT staInbt = stackCompoundNBT.get(nbtId);

						if(staInbt == null || !staInbt.equals(ingInbt)) {
							return false;
						}
					}

					return true;
				}
			} else {
				return true;
			}
		}

		return false;
	}
	
	@SuppressWarnings("deprecation")
	@Nonnull
	public JsonElement serialize() {
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("item", Registry.ITEM.getKey(this.acceptedItem.getItem()).toString());

		if(this.ingredientNbt != null) {
			jsonObject.addProperty("nbt", this.ingredientNbt.toString());
		}

		return jsonObject;
	}
	
	@Nonnull
	public MMTIngredientSerializer getSerializer() {
		return MMTIngredientSerializer.INSTANCE;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof MMTIngredient) {
			MMTIngredient other = (MMTIngredient) obj;
			
			if(this.acceptedItem.getItem() == other.acceptedItem.getItem()) {
				if(this.ingredientNbt != null) {
					if(other.ingredientNbt != null) {
						for(String nbtId : this.ingredientNbt.keySet()) {
							INBT ingInbt = this.ingredientNbt.get(nbtId);
							INBT othInbt = other.ingredientNbt.get(nbtId);
							
							if(othInbt == null || !othInbt.equals(ingInbt)) {
								return false;
							}
						}
						
						return true;
					}
				} else {
					return true;
				}
			}
		}
		
		return false;
	}
}