package yasmin.modularmultitool.items.modules.laser;

import yasmin.modularmultitool.config.Settings;
import yasmin.modularmultitool.items.base.ItemLaserModuleBase;

public class ItemMiningLaserModule extends ItemLaserModuleBase {
	public ItemMiningLaserModule() {
		super();
	}
	
	@Override
	public boolean isEnabled() {
		return Settings.itemMiningLaserModule;
	}
}