package yasmin.modularmultitool.items.base;

public class ItemEnergyModuleBase extends ItemModuleBase {
	public ItemEnergyModuleBase() {
		super();
	}
	
	@Override
	public int getMaxLevel() {
		return 5;
	}
}