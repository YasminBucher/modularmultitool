package yasmin.modularmultitool.recipes;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;

public class MMTCraftingResult {
	private final ItemStack stack;
	private CompoundNBT resultNbt;
	
	public MMTCraftingResult(ItemStack stackIn, CompoundNBT resultNbtIn) {
		this.stack = stackIn;
		this.resultNbt = resultNbtIn;
		this.stack.setTag(resultNbtIn);
	}
	
	public ItemStack getStack() {
		return this.stack;
	}
	
	public CompoundNBT getResultNbt() {
		return this.resultNbt;
	}
	
	public MMTCraftingResult setNbt(CompoundNBT compoundNBT) {
		this.resultNbt = compoundNBT;
		this.stack.setTag(compoundNBT);
		return this;
	}
}