package yasmin.modularmultitool.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import yasmin.modularmultitool.machines.MultiToolMachineTileEntity;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class MultiToolMachineBlock extends Block {
	public static final float HARDNESS_AND_RESISTANCE = 10.0F;
	public static final int LIGHT_VALUE_IN = 14;
	
	public MultiToolMachineBlock() {
		super(Block.Properties.create(Material.IRON).sound(SoundType.METAL).hardnessAndResistance(HARDNESS_AND_RESISTANCE).setLightLevel((state) -> LIGHT_VALUE_IN));
	}
	
	@Override
	public boolean hasTileEntity(BlockState state) {
		return true;
	}
	
	@Nullable
	@Override
	public TileEntity createTileEntity(BlockState state, IBlockReader world) {
		return new MultiToolMachineTileEntity();
	}
	
	@Nullable
	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		return this.getDefaultState().with(BlockStateProperties.FACING, context.getPlacementHorizontalFacing().getOpposite());
	}
	
	@Override
	protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
		builder.add(BlockStateProperties.FACING);
	}
	
	@SuppressWarnings("deprecation")
	@Nonnull
	@Override
	public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult p_225533_6_) {
		if(worldIn.isRemote) {
			return ActionResultType.SUCCESS;
		}
		
		INamedContainerProvider provider = this.getContainer(state, worldIn, pos);
		
		if(provider != null) {
			player.openContainer(provider);
		}
		
		return ActionResultType.SUCCESS;
	}
	
	@SuppressWarnings("deprecation")
	@Nullable
	@Override
	public INamedContainerProvider getContainer(BlockState state, World worldIn, BlockPos pos) {
		TileEntity te = worldIn.getTileEntity(pos);
		
		if(te instanceof MultiToolMachineTileEntity) {
			return (INamedContainerProvider)te;
		}
		
		return null;
	}
}