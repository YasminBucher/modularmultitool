package yasmin.modularmultitool.items.modules.tools;

import net.minecraft.item.Item;
import net.minecraft.item.ItemUseContext;
import net.minecraft.item.Items;
import net.minecraft.util.ActionResultType;
import net.minecraftforge.common.ToolType;
import yasmin.modularmultitool.config.Settings;
import yasmin.modularmultitool.items.base.ItemToolModuleBase;

import javax.annotation.Nonnull;

public class ItemShovelModule extends ItemToolModuleBase {
	public ItemShovelModule() {
		super(ToolType.SHOVEL, 5.0F);
	}
	
	@Nonnull
	public ActionResultType onItemUse(ItemUseContext context) {
		return Items.DIAMOND_SHOVEL.onItemUse(context);
	}
	
	@Override
	public Item getBaseItem() {
		return Items.WOODEN_SHOVEL;
	}
	
	@Override
	public boolean isEnabled() {
		return Settings.itemShovelModule;
	}
}