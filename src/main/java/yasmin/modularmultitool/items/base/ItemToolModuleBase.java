package yasmin.modularmultitool.items.base;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;
import net.minecraftforge.common.ToolType;
import yasmin.modularmultitool.items.ModItems;
import yasmin.modularmultitool.util.ItemUtil;
import yasmin.modularmultitool.util.TextComponentUtil;

import java.util.List;
import java.util.function.Consumer;

public class ItemToolModuleBase extends ItemModuleBase {
	public ToolType toolType;
	public float strVsProperBlocks;
	
	public ItemToolModuleBase(ToolType toolType, float strVsProperBlocks) {
		super(ModItems.propertiesModule);
		
		this.toolType = toolType;
		this.strVsProperBlocks = strVsProperBlocks;
	}
	
	public Item getBaseItem() {
		return Items.AIR;
	}
	
	public ToolType getToolType() {
		return this.toolType;
	}
	
	@Override
	public int getMaxLevel() {
		return 5;
	}
	
	@Override
	public int getDamage(ItemStack stack) {
		return ItemUtil.getMaxDurability(stack) - ItemUtil.getDurability(stack);
	}
	
	@Override
	public boolean showDurabilityBar(ItemStack stack) {
		return this.isDamaged(stack);
	}
	
	@Override
	public double getDurabilityForDisplay(ItemStack stack) {
		return 1.0d - ItemUtil.getDurabilityPercentage(stack);
	}
	
	@Override
	public int getMaxDamage(ItemStack stack) {
		return ItemUtil.getMaxDurability(stack);
	}
	
	@Override
	public boolean isDamaged(ItemStack stack) {
		return ItemUtil.getDurabilityPercentage(stack) < 1.0d;
	}
	
	@Override
	public void setDamage(ItemStack stack, int damage) {
		int durability = this.getMaxDamage(stack) - damage;
		ItemUtil.setDurability(stack, durability);
	}
	
	@Override
	public <T extends LivingEntity> int damageItem(ItemStack stack, int amount, T entity, Consumer<T> onBroken) {
		return 0;
	}
	
	@Override
	public boolean isDamageable() {
		return true;
	}
	
	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		super.addInformation(stack, worldIn, tooltip, flagIn);
		tooltip.add(TextComponentUtil.getModuleDisplayText(stack));
	}
}