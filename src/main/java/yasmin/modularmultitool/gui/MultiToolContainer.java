package yasmin.modularmultitool.gui;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import yasmin.modularmultitool.inventory.MultiToolInventory;

import javax.annotation.Nonnull;

public class MultiToolContainer extends Container {
	public static final int X_POSITION_LEFT = 18;
	public static final int X_POSITION_RIGHT = 141;
	public static final int Y_POS_DISTANCE = 20;
	public static final int Y_POS_OFFSET = 7;
	public static final int Y_POSITION_HOTBAR = 251;
	public static final int Y_POSITION_PLAYER_INVENTORY_START = 193;
	public static final int SLOT_SIZE = 18;
	public static final int PLAYER_INVENTORY_SLOT_COUNT = 36;
	
	private MultiToolInventory multiToolInventory;
	
	public MultiToolContainer(int id, PlayerInventory playerInv) {
		this(id, playerInv, new MultiToolInventory(playerInv.player.getHeldItemMainhand()));
	}
	
	public MultiToolContainer(int id, PlayerInventory playerInv, MultiToolInventory multiToolInventory) {
		super(ModContainerTypes.CONTAINERTYPE_MULTITOOL.get(), id);
		this.multiToolInventory = multiToolInventory;
		
		// Multitool Inventory, Slot 0 - 15, Slot IDs 0 - 15
		for(int i = 0; i < 8; i++) {
			this.addSlot(new SingleItemSlot(this.multiToolInventory, i, X_POSITION_LEFT, i * Y_POS_DISTANCE + Y_POS_OFFSET));
			this.addSlot(new SingleItemSlot(this.multiToolInventory, i + 8, X_POSITION_RIGHT, i * Y_POS_DISTANCE + Y_POS_OFFSET));
		}
		
		// Player Inventory, Slot 9 - 35, Slot IDs 9 - 35
		for(int y = 0; y < 3; y++) {
			for(int x = 0; x < 9; x++) {
				this.addSlot(new Slot(playerInv, x + y * 9 + 9, 7 + x * SLOT_SIZE, Y_POSITION_PLAYER_INVENTORY_START + y * SLOT_SIZE));
			}
		}
		
		// Player Inventory, Slot 0 - 8, Slot IDs 36 - 44 (Hotbar)
		for(int x = 0; x < 9; x++) {
			this.addSlot(new Slot(playerInv, x, 7 + x * SLOT_SIZE, Y_POSITION_HOTBAR));
		}
	}
	
	@Nonnull
	@Override
	public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
		ItemStack previous = ItemStack.EMPTY;
		Slot slot = this.inventorySlots.get(index);
		
		if(slot != null && slot.getHasStack()) {
			ItemStack current = slot.getStack();
			previous = current.copy();
			
			if(index < MultiToolInventory.INV_SIZE) {
				if(!this.mergeItemStack(current, MultiToolInventory.INV_SIZE, MultiToolInventory.INV_SIZE + PLAYER_INVENTORY_SLOT_COUNT, true)) {
					return ItemStack.EMPTY;
				}
			} else {
				if(!this.mergeItemStack(current, 0, MultiToolInventory.INV_SIZE, false)) {
					return ItemStack.EMPTY;
				}
			}
			
			if(current.getCount() == 0) {
				slot.putStack(ItemStack.EMPTY);
			} else {
				slot.onSlotChanged();
			}
			
			if(current.getCount() == previous.getCount()) {
				return ItemStack.EMPTY;
			}
			
			slot.onTake(playerIn, current);
		}
		
		return previous;
	}
	
	@Override
	public boolean canInteractWith(@Nonnull PlayerEntity playerIn) {
		return this.multiToolInventory.isUsableByPlayer(playerIn);
	}
	
	@Override
	protected boolean mergeItemStack(ItemStack stack, int startIndex, int endIndex, boolean reverseDirection) {
		boolean success = false;
		int index = startIndex;
		
		if(reverseDirection) {
			index = endIndex - 1;
		}
		
		Slot slot;
		ItemStack stackinslot;
		
		if(stack.isStackable()) {
			while(stack.getCount() > 0 && (!reverseDirection && index < endIndex || reverseDirection && index >= startIndex)) {
				slot = this.inventorySlots.get(index);
				stackinslot = slot.getStack();
				
				if(!stackinslot.isEmpty() && stackinslot.getItem() == stack.getItem() && ItemStack.areItemStackTagsEqual(stack, stackinslot)) {
					int newStackCount = stackinslot.getCount() + stack.getCount();
					int maxsize = Math.min(stack.getMaxStackSize(), slot.getItemStackLimit(stack));
					
					if(newStackCount <= maxsize) {
						stack.setCount(0);
						stackinslot.setCount(1);
						slot.onSlotChanged();
						success = true;
					} else if(stackinslot.getCount() < maxsize) {
						stack.setCount(stack.getCount() - stack.getMaxStackSize() - stackinslot.getCount());
						stackinslot.setCount(stack.getMaxStackSize());
						slot.onSlotChanged();
						success = true;
					}
				}
				
				if(reverseDirection) {
					index--;
				} else {
					index++;
				}
			}
		}
		
		if(stack.getCount() > 0) {
			if(reverseDirection) {
				index = endIndex - 1;
			} else {
				index = startIndex;
			}
			
			while(!reverseDirection && index < endIndex || reverseDirection && index >= startIndex && stack.getCount() > 0) {
				slot = this.inventorySlots.get(index);
				stackinslot = slot.getStack();
				
				if(stackinslot.isEmpty() && slot.isItemValid(stack)) {
					if(stack.getCount() < slot.getItemStackLimit(stack)) {
						slot.putStack(stack.copy());
						stack.setCount(0);
						success = true;
						break;
					} else {
						ItemStack newstack = stack.copy();
						newstack.setCount(slot.getItemStackLimit(stack));
						slot.putStack(newstack);
						stack.setCount(stack.getCount() - slot.getItemStackLimit(stack));
						success = true;
					}
				}
				
				if(reverseDirection) {
					index--;
				} else {
					index++;
				}
			}
		}
		
		return success;
	}
}