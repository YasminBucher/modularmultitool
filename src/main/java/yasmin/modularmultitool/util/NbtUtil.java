package yasmin.modularmultitool.util;

import net.minecraft.nbt.CompoundNBT;

public class NbtUtil {
	public static CompoundNBT createNbtWithLevel(int level) {
		CompoundNBT compoundNBT = new CompoundNBT();
		compoundNBT.putInt("mmt_level", level);
		return compoundNBT;
	}
}