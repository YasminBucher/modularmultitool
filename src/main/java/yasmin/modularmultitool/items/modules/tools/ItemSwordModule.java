package yasmin.modularmultitool.items.modules.tools;

import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraftforge.common.ToolType;
import yasmin.modularmultitool.config.Settings;
import yasmin.modularmultitool.items.base.ItemToolModuleBase;

public class ItemSwordModule extends ItemToolModuleBase {
	public static final ToolType TT_SWORD = ToolType.get("sword");
	
	public ItemSwordModule() {
		super(TT_SWORD, 5.0F);
	}
	
	@Override
	public Item getBaseItem() {
		return Items.WOODEN_SWORD;
	}
	
	@Override
	public boolean isEnabled() {
		return Settings.itemSwordModule;
	}
}