package yasmin.modularmultitool.recipes;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementRewards;
import net.minecraft.advancements.ICriterionInstance;
import net.minecraft.advancements.IRequirementsStrategy;
import net.minecraft.advancements.criterion.RecipeUnlockedTrigger;
import net.minecraft.data.IFinishedRecipe;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;

public class MMTShapedRecipeBuilder {
	private final MMTCraftingResult craftingResult;
	private final int count;
	private final List<String> pattern = Lists.newArrayList();
	private final Map<Character, MMTIngredient> key = Maps.newLinkedHashMap();
	private final Advancement.Builder advancementBuilder = Advancement.Builder.builder();
	
	public MMTShapedRecipeBuilder(MMTCraftingResult resultIn, int countIn) {
		this.craftingResult = resultIn;
		this.count = countIn;
	}
	
	public static MMTShapedRecipeBuilder shapedRecipe(MMTCraftingResult craftingResultIn) {
		return shapedRecipe(craftingResultIn, 1);
	}
	
	public static MMTShapedRecipeBuilder shapedRecipe(MMTCraftingResult craftingResultIn, int countIn) {
		return new MMTShapedRecipeBuilder(craftingResultIn, countIn);
	}
	
	public MMTShapedRecipeBuilder key(Character symbol, MMTIngredient ingredientIn) {
		if(this.key.containsKey(symbol)) {
			throw new IllegalArgumentException("Symbol '" + symbol + "' is already defined!");
		} else if(symbol == ' ') {
			throw new IllegalArgumentException("Symbol ' ' (whitespace) is reserved and cannot be defined");
		} else {
			this.key.put(symbol, ingredientIn);
			return this;
		}
	}
	
	public MMTShapedRecipeBuilder patternLine(String patternIn) {
		if(!this.pattern.isEmpty() && patternIn.length() != this.pattern.get(0).length()) {
			throw new IllegalArgumentException("Pattern must be the same width on every line!");
		} else {
			this.pattern.add(patternIn);
			return this;
		}
	}
	
	public MMTShapedRecipeBuilder addCriterion(String name, ICriterionInstance criterionIn) {
		this.advancementBuilder.withCriterion(name, criterionIn);
		return this;
	}
	
	public void build(Consumer<IFinishedRecipe> consumerIn) {
		this.build(consumerIn, "");
	}
	
	@SuppressWarnings("deprecation")
	public void build(Consumer<IFinishedRecipe> consumerIn, String resourceLocationSuffix) {
		this.build(consumerIn, new ResourceLocation(Registry.ITEM.getKey(this.craftingResult.getStack().getItem()).toString() + resourceLocationSuffix));
	}
	
	public void build(Consumer<IFinishedRecipe> consumerIn, ResourceLocation id) {
		this.validate(id);
		this.advancementBuilder
				.withParentId(new ResourceLocation("recipes/root"))
				.withCriterion("has_the_recipe", RecipeUnlockedTrigger.create(id))
				.withRewards(AdvancementRewards.Builder.recipe(id))
				.withRequirementsStrategy(IRequirementsStrategy.OR);

		ResourceLocation tmpAdvancementId = new ResourceLocation(id.getNamespace(), "recipes/" + Objects.requireNonNull(this.craftingResult.getStack().getItem().getGroup()).getPath() + "/" + id.getPath());
		consumerIn.accept(new MMTFinishedRecipe(id, this.craftingResult, this.count, "", this.pattern, this.key, this.advancementBuilder, tmpAdvancementId));
	}
	
	private void validate(ResourceLocation id) {
		if(this.pattern.isEmpty()) {
			throw new IllegalStateException("No pattern is defined for shaped recipe " + id + "!");
		} else {
			Set<Character> set = Sets.newHashSet(this.key.keySet());
			set.remove(' ');

			for(String s : this.pattern) {
				for(int i = 0; i < s.length(); ++i) {
					char c0 = s.charAt(i);
					if(!this.key.containsKey(c0) && c0 != ' ') {
						throw new IllegalStateException("Pattern in recipe " + id + " uses undefined symbol '" + c0 + "'");
					}

					set.remove(c0);
				}
			}

			if(!set.isEmpty()) {
				throw new IllegalStateException("Ingredients are defined but not used in pattern for recipe " + id);
			} else if(this.pattern.size() == 1 && this.pattern.get(0).length() == 1) {
				throw new IllegalStateException("Shaped recipe " + id + " only takes in a single item - should it be a shapeless recipe instead?");
			} else if(this.advancementBuilder.getCriteria().isEmpty()) {
				throw new IllegalStateException("No way of obtaining recipe " + id);
			}
		}
	}
}