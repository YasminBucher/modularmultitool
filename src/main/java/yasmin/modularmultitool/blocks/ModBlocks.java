package yasmin.modularmultitool.blocks;

import net.minecraft.block.Block;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import yasmin.modularmultitool.ModularMultiTool;

public class ModBlocks {
	public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, ModularMultiTool.MODID);
	
	public static final RegistryObject<Block> AUTOSMELTFAKE = BLOCKS.register("block_autosmeltfake", AutosmeltFakeBlock::new);
	public static final RegistryObject<Block> MULTITOOLMACHINE = BLOCKS.register("block_multitoolmachine", MultiToolMachineBlock::new);
	
	public static void registerBlocks() {
		BLOCKS.register(FMLJavaModLoadingContext.get().getModEventBus());
	}
}