package yasmin.modularmultitool.util;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

public class DebugUtil {
	public static void notifyPlayer(ServerPlayerEntity player, World world, String msg) {
		if(!world.isRemote) {
			player.sendMessage(new StringTextComponent(msg), null);
		}
	}
}