package yasmin.modularmultitool.items.modules.energy;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import yasmin.modularmultitool.config.Settings;
import yasmin.modularmultitool.energy.ItemEnergyStorageProvider;
import yasmin.modularmultitool.items.base.ItemEnergyModuleBase;
import yasmin.modularmultitool.util.ItemUtil;
import yasmin.modularmultitool.util.TextComponentUtil;

import java.util.List;

public class ItemBatteryModule extends ItemEnergyModuleBase {
	public ItemBatteryModule() {
		super();
	}
	
	@Override
	public boolean isEnabled() {
		return Settings.itemBatteryModule;
	}
	
	@Override
	public void addInformation(ItemStack stack, World world, List<ITextComponent> tooltip, ITooltipFlag flag) {
		super.addInformation(stack, world, tooltip, flag);
		
		tooltip.add(TextComponentUtil.getModuleDisplayText(stack));
	}
	
	@Override
	public boolean showDurabilityBar(ItemStack stack) {
		return ItemUtil.getEnergyPercentange(stack) < 1.0d;
	}
	
	@Override
	public double getDurabilityForDisplay(ItemStack stack) {
		return 1.0d - ItemUtil.getEnergyPercentange(stack);
	}
	
	@Override
	public ICapabilityProvider initCapabilities(ItemStack stack, CompoundNBT nbt) {
		return new ItemEnergyStorageProvider(stack, this);
	}
}