package yasmin.modularmultitool.gui;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import yasmin.modularmultitool.ModularMultiTool;

public class MultiToolMachineContainerScreen extends ContainerScreen<MultiToolMachineContainer> {
	private final static ResourceLocation MULTITOOLMACHINE_INVENTORY_TEXTURE = new ResourceLocation(ModularMultiTool.MODID, "textures/gui/gui_multitoolmachine.png");
	public static final int X_SIZE = 176;
	public static final int Y_SIZE = 162;
	public static final int COLOR = 0x222222;
	public static final int Y_POS_TITLE = -20;
	public static final float FTWO = 2F;
	
	public MultiToolMachineContainerScreen(MultiToolMachineContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
		super(screenContainer, inv, titleIn);
		
		this.xSize = X_SIZE;
		this.ySize = Y_SIZE;
	}
	
	@Override
	public void render(MatrixStack mstack, int p_render_1_, int p_render_2_, float p_render_3_) {
		this.renderBackground(mstack);
		super.render(mstack, p_render_1_, p_render_2_, p_render_3_);
		this.renderHoveredTooltip(mstack, p_render_1_, p_render_2_);
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(MatrixStack mstack, int mouseX, int mouseY) {
		String text = "Modular MultiTool Machine";
		this.font.drawString(mstack, text, this.xSize / FTWO - this.font.getStringWidth(text) / FTWO, Y_POS_TITLE, COLOR);
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(MatrixStack mstack, float partialTicks, int mouseX, int mouseY) {
		RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
		
		assert this.minecraft != null;
		this.minecraft.getTextureManager().bindTexture(MULTITOOLMACHINE_INVENTORY_TEXTURE);
		this.blit(mstack, this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);
	}
}
