package yasmin.modularmultitool.gui;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.Slot;
import yasmin.modularmultitool.machines.MultiToolMachineTileEntity;

import javax.annotation.Nonnull;

public class MultiToolMachineContainer extends Container {
	public static final int X_POS_CRAFTING_GRID_START = 26;
	public static final int Y_POS_CRAFTING_GRID_START = 12;
	public static final int X_Y_CRAFTING_GRID_SLOT_DISTANCE = 21;
	public static final int X_POS_CRAFTING_OUTPUT = 134;
	public static final int Y_POS_CRAFTING_OUTPUT = 33;
	public static final int SLOT_SIZE = 18;
	public static final int Y_POS_PLAYER_INV_START = 80;
	public static final int X_POST_PLAYER_INV_START = 8;
	public static final int Y_POS_PLAYER_HOTBAR = 138;
	
	private final MultiToolMachineTileEntity inventory;
	private final PlayerInventory playerInventory;
	
	public MultiToolMachineContainer(int id, PlayerInventory inventory) {
		this(id, inventory, new MultiToolMachineTileEntity());
	}
	
	public MultiToolMachineContainer(int id, PlayerInventory playerInventory, MultiToolMachineTileEntity machineInventory) {
		super(ModContainerTypes.CONTAINERTYPE_MULTITOOLMACHINE.get(), id);
		this.inventory = machineInventory;
		this.playerInventory = playerInventory;
		this.setupInventorySlots();
	}
	
	private void setupInventorySlots() {
		// Multitool Machine Inventory, Slot 0 - 10, Slot IDs 0 - 10
		for(int y = 0; y < 3; y++) {
			for(int x = 0; x < 3; x++) {
				this.addSlot(new Slot(this.inventory, x + y * 3, X_POS_CRAFTING_GRID_START + x * X_Y_CRAFTING_GRID_SLOT_DISTANCE, Y_POS_CRAFTING_GRID_START + y * X_Y_CRAFTING_GRID_SLOT_DISTANCE));
			}
		}
		
		this.addSlot(new MultiToolMachineOutputSlot(this.inventory, 9, X_POS_CRAFTING_OUTPUT, Y_POS_CRAFTING_OUTPUT));
		
		// Player Inventory, Slot 9 - 35, Slot IDs 9 - 35
		for(int y = 0; y < 3; y++) {
			for(int x = 0; x < 9; x++) {
				this.addSlot(new Slot(this.playerInventory, x + y * 9 + 9, X_POST_PLAYER_INV_START + x * SLOT_SIZE, Y_POS_PLAYER_INV_START + y * SLOT_SIZE));
			}
		}
		
		// Player Inventory, Slot 0 - 8, Slot IDs 36 - 44 (Hotbar)
		for(int x = 0; x < 9; x++) {
			this.addSlot(new Slot(this.playerInventory, x, X_POST_PLAYER_INV_START + x * SLOT_SIZE, Y_POS_PLAYER_HOTBAR));
		}
	}
	
	@Override
	public boolean canInteractWith(@Nonnull PlayerEntity playerIn) {
		return true;
	}
}
