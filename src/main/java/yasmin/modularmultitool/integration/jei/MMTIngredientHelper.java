package yasmin.modularmultitool.integration.jei;

import mezz.jei.api.ingredients.IIngredientHelper;
import yasmin.modularmultitool.ModularMultiTool;
import yasmin.modularmultitool.recipes.MMTIngredient;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Objects;

public class MMTIngredientHelper implements IIngredientHelper<MMTIngredient> {
	@Nullable
	@Override
	public MMTIngredient getMatch(@Nonnull Iterable<MMTIngredient> iterable, @Nonnull MMTIngredient ingredient) {
		MMTIngredient match = null;
		
		for(MMTIngredient ing : iterable) {
			if(ing.equals(ingredient)) {
				match = ing;
				break;
			}
		}
		
		return match;
	}
	
	@Nonnull
	@Override
	public String getDisplayName(@Nonnull MMTIngredient ingredient) {
		return ingredient.getAcceptedItem().getDisplayName().getString();
	}
	
	@Nonnull
	@Override
	public String getUniqueId(@Nonnull MMTIngredient ingredient) {
		return Objects.requireNonNull(ingredient.getAcceptedItem().getItem().getRegistryName()).toString();
	}
	
	@Nonnull
	@Override
	public String getWildcardId(@Nonnull MMTIngredient ingredient) {
		return getUniqueId(ingredient);
	}
	
	@Nonnull
	@Override
	public String getModId(@Nonnull MMTIngredient ingredient) {
		return ModularMultiTool.MODID;
	}
	
	@Nonnull
	@Override
	public String getResourceId(@Nonnull MMTIngredient ingredient) {
		return getUniqueId(ingredient);
	}
	
	@Nonnull
	@Override
	public MMTIngredient copyIngredient(@Nonnull MMTIngredient ingredient) {
		return ingredient;
	}
	
	@Nonnull
	@Override
	public String getErrorInfo(@Nullable MMTIngredient ingredient) {
		assert ingredient != null;
		return getUniqueId(ingredient);
	}
}
