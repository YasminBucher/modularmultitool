package yasmin.modularmultitool.gui;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import yasmin.modularmultitool.items.base.ItemModuleBase;

import javax.annotation.Nullable;

public class SingleItemSlot extends Slot {
	public SingleItemSlot(IInventory inventoryIn, int index, int xPosition, int yPosition) {
		super(inventoryIn, index, xPosition, yPosition);
	}
	
	@Override
	public boolean isItemValid(@Nullable ItemStack stack) {
		assert stack != null;
		
		if(!stack.isEmpty() && stack.getItem() instanceof ItemModuleBase) {
			return this.inventory.isItemValidForSlot(this.getSlotIndex(), stack);
		}
		
		return false;
	}
	
	@Override
	public int getSlotStackLimit() {
		return 1;
	}
}