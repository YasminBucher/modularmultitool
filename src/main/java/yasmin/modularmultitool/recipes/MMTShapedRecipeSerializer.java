package yasmin.modularmultitool.recipes;

import com.google.gson.JsonObject;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;

public class MMTShapedRecipeSerializer extends net.minecraftforge.registries.ForgeRegistryEntry<IRecipeSerializer<?>> implements IRecipeSerializer<MMTShapedRecipe> {
	@Nonnull
	@Override
	public MMTShapedRecipe read(@Nonnull ResourceLocation recipeId, @Nonnull JsonObject json) {
		String group = JSONUtils.getString(json, "group", "");
		Map<String, MMTIngredient> map = MMTShapedRecipe.deserializeKey(JSONUtils.getJsonObject(json, "key"));
		String[] astring = MMTShapedRecipe.shrink(MMTShapedRecipe.patternFromJson(JSONUtils.getJsonArray(json, "pattern")));
		int recipeWidth = astring[0].length();
		int recipeHeight = astring.length;
		NonNullList<MMTIngredient> ingredients = MMTShapedRecipe.deserializeIngredients(astring, map, recipeWidth, recipeHeight);
		MMTCraftingResult result = MMTShapedRecipe.deserializeCraftingResult(JSONUtils.getJsonObject(json, "result"));
		return new MMTShapedRecipe(recipeId, group, recipeWidth, recipeHeight, ingredients, result);
	}
	
	@Nullable
	@Override
	public MMTShapedRecipe read(@Nonnull ResourceLocation recipeId, PacketBuffer buffer) {
		int i = buffer.readVarInt();
		int j = buffer.readVarInt();
		String s = buffer.readString(32767);
		NonNullList<MMTIngredient> nonnulllist = NonNullList.withSize(i * j, MMTIngredient.EMPTY);
		
		for(int k = 0; k < nonnulllist.size(); ++k) {
			nonnulllist.set(k, MMTIngredient.read(buffer));
		}
		
		ItemStack itemstack = buffer.readItemStack();
		CompoundNBT resultNbt = buffer.readCompoundTag();
		
		MMTCraftingResult result = new MMTCraftingResult(itemstack, resultNbt);
		
		return new MMTShapedRecipe(recipeId, s, i, j, nonnulllist, result);
	}
	
	@Override
	public void write(PacketBuffer buffer, MMTShapedRecipe recipe) {
		buffer.writeVarInt(recipe.recipeWidth);
		buffer.writeVarInt(recipe.recipeHeight);
		buffer.writeString(recipe.group);
		
		for(Ingredient ingredient : recipe.recipeItems) {
			ingredient.write(buffer);
		}
		
		buffer.writeItemStack(recipe.craftingResult.getStack());
		buffer.writeCompoundTag(recipe.craftingResult.getResultNbt());
	}
}