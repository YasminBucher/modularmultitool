package yasmin.modularmultitool.gui;

import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.registry.Registry;
import net.minecraftforge.common.extensions.IForgeContainerType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import yasmin.modularmultitool.ModularMultiTool;

public class ModContainerTypes {
	public static final DeferredRegister<ContainerType<?>> CONTAINERS = DeferredRegister.create(ForgeRegistries.CONTAINERS, ModularMultiTool.MODID);
	public static final RegistryObject<ContainerType<MultiToolContainer>> CONTAINERTYPE_MULTITOOL = CONTAINERS.register("containertype_multitool", () -> IForgeContainerType.create(((windowId, inv, data) -> new MultiToolContainer(windowId, inv))));
	public static final RegistryObject<ContainerType<MultiToolMachineContainer>> CONTAINERTYPE_MULTITOOLMACHINE = CONTAINERS.register("containertype_multitoolmachine", () -> IForgeContainerType.create(((windowId, inv, data) -> new MultiToolMachineContainer(windowId, inv))));
	//public static final ContainerType<MultiToolContainer> CONTAINERTYPE_MULTITOOL = registerContainerType("containertype_multitool", MultiToolContainer::new);
	//public static final ContainerType<MultiToolMachineContainer> CONTAINERTYPE_MULTITOOLMACHINE = registerContainerType("containertype_multitoolmachine", MultiToolMachineContainer::new);
	
	/*private static <T extends Container> ContainerType<T> registerContainerType(String key, ContainerType.IFactory<T> factory) {
		return Registry.register(Registry.MENU, key, new ContainerType<>(factory));
	}*/
	
	public static void registerContainerTypes() {
		CONTAINERS.register(FMLJavaModLoadingContext.get().getModEventBus());
	}
}