package yasmin.modularmultitool.recipes;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.minecraft.advancements.Advancement;
import net.minecraft.data.IFinishedRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;

public class MMTFinishedRecipe implements IFinishedRecipe {
	private final ResourceLocation id;
	private final MMTCraftingResult craftingResult;
	private final int count;
	private final String group;
	private final List<String> pattern;
	private final Map<Character, MMTIngredient> key;
	private final Advancement.Builder advancementBuilder;
	private final ResourceLocation advancementId;
	
	public MMTFinishedRecipe(ResourceLocation idIn, MMTCraftingResult resultIn, int countIn, String groupIn, List<String> patternIn, Map<Character, MMTIngredient> keyIn, Advancement.Builder advancementBuilderIn, ResourceLocation advancementIdIn) {
		this.id = idIn;
		this.craftingResult = resultIn;
		this.count = countIn;
		this.group = groupIn;
		this.pattern = patternIn;
		this.key = keyIn;
		this.advancementBuilder = advancementBuilderIn;
		this.advancementId = advancementIdIn;
	}
	
	@Override
	public void serialize(@Nonnull JsonObject json) {
		if(!this.group.isEmpty()) {
			json.addProperty("group", this.group);
		}
		
		JsonArray jsonArray = new JsonArray();
		
		for(String s : this.pattern) {
			jsonArray.add(s);
		}
		
		json.add("pattern", jsonArray);
		
		JsonObject jsonObjectKey = new JsonObject();
		
		for(Map.Entry<Character, MMTIngredient> entry : this.key.entrySet()) {
			jsonObjectKey.add(String.valueOf(entry.getKey()), entry.getValue().serialize());
		}
		
		json.add("key", jsonObjectKey);
		
		JsonObject jsonObjectResult = new JsonObject();
		//noinspection deprecation
		jsonObjectResult.addProperty("item", Registry.ITEM.getKey(this.craftingResult.getStack().getItem()).toString());
		
		if(this.craftingResult.getResultNbt() != null) {
			jsonObjectResult.addProperty("nbt", this.craftingResult.getResultNbt().toString());
		}
		
		if(this.count > 1) {
			jsonObjectResult.addProperty("count", this.count);
		}
		
		json.add("result", jsonObjectResult);
	}
	
	@Nonnull
	@Override
	public ResourceLocation getID() {
		return this.id;
	}
	
	@Nonnull
	@Override
	public IRecipeSerializer<?> getSerializer() {
		return ModRecipes.SHAPED_WITH_NBT.get();
	}
	
	@Nullable
	@Override
	public JsonObject getAdvancementJson() {
		return this.advancementBuilder.serialize();
	}
	
	@Nullable
	@Override
	public ResourceLocation getAdvancementID() {
		return this.advancementId;
	}
}