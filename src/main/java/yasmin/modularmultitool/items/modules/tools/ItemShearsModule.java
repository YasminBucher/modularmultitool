package yasmin.modularmultitool.items.modules.tools;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.IForgeShearable;
import net.minecraftforge.common.ToolType;
import yasmin.modularmultitool.config.Settings;
import yasmin.modularmultitool.items.base.ItemToolModuleBase;

import javax.annotation.Nonnull;
import java.util.List;

public class ItemShearsModule extends ItemToolModuleBase {
	public static final ToolType TT_SHEARS = ToolType.get("shears");
	
	public ItemShearsModule() {
		super(TT_SHEARS, 5.0F);
	}
	
	@Override
	@Nonnull
	public ActionResultType itemInteractionForEntity(ItemStack stack, PlayerEntity player, LivingEntity target, Hand hand) {
		if(target instanceof IForgeShearable) {
			IForgeShearable shearableTarget = (IForgeShearable) target;
			BlockPos pos = target.getPosition();
			
			if(shearableTarget.isShearable(stack, target.world, pos)) {
				List<ItemStack> drops = shearableTarget.onSheared(player, stack, target.world, pos, EnchantmentHelper.getEnchantmentLevel(Enchantments.FORTUNE, stack));
				
				for(ItemStack drop : drops) {
					target.entityDropItem(drop, 1.0F);
				}
			}
			
			return ActionResultType.SUCCESS;
		}
		
		return ActionResultType.PASS;
	}
	
	@Override
	public Item getBaseItem() {
		return Items.SHEARS;
	}
	
	@Override
	public boolean isEnabled() {
		return Settings.itemShearsModule;
	}
}