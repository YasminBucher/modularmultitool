package yasmin.modularmultitool.util;

import net.minecraft.item.ItemStack;
import net.minecraft.util.text.*;
import yasmin.modularmultitool.items.base.*;
import yasmin.modularmultitool.items.modules.energy.ItemBatteryModule;

public class TextComponentUtil {
	private static final Style STYLE_DEFAULT = Style.EMPTY.setColor(Color.fromTextFormatting(TextFormatting.RESET));
	private static final Style STYLE_GOLD = Style.EMPTY.setColor(Color.fromTextFormatting(TextFormatting.GOLD));
	private static final Style STYLE_RED = Style.EMPTY.setColor(Color.fromTextFormatting(TextFormatting.RED));
	private static final Style STYLE_LIGHT_PURPLE = Style.EMPTY.setColor(Color.fromTextFormatting(TextFormatting.LIGHT_PURPLE));
	private static final Style STYLE_DARK_AQUA = Style.EMPTY.setColor(Color.fromTextFormatting(TextFormatting.DARK_AQUA));
	
	public static IFormattableTextComponent getModuleDisplayText(ItemStack stack) {
		IFormattableTextComponent displayText = new StringTextComponent("");
		
		if(stack.getItem() instanceof ItemModuleBase) {
			Style style = STYLE_DEFAULT;
			String prefix = "";
			ItemModuleBase module = (ItemModuleBase) stack.getItem();
			
			if(module instanceof ItemToolModuleBase) {
				style = STYLE_GOLD;
				prefix = "[T] ";
			}
			
			if(module instanceof ItemUpgradeModuleBase) {
				style = STYLE_LIGHT_PURPLE;
				prefix = "[U] ";
			}
			
			if(module instanceof ItemLaserModuleBase) {
				style = STYLE_DARK_AQUA;
				prefix = "[L] ";
			}
			
			if(module instanceof ItemEnergyModuleBase) {
				style = STYLE_RED;
				prefix = "[E] ";
			}
			
			displayText = displayText.append(new StringTextComponent(prefix)).append(module.getDisplayName(stack)).append(new StringTextComponent(String.format(" Lv. %d", ItemUtil.getLevel(stack)))).setStyle(style);
			
			if(module instanceof ItemBatteryModule) {
				displayText.append(new StringTextComponent(" - ")).append(getEnergyDisplayText(stack));
			}
			
			if(module instanceof ItemToolModuleBase) {
				displayText.append(new StringTextComponent(" - ")).append(getDurabilityDisplay(stack));
			}
		}
		
		return displayText;
	}
	
	private static ITextComponent getEnergyDisplayText(ItemStack stack) {
		int currentEnergy = ItemUtil.getCurrentEnergy(stack);
		int maxEnergy = ItemUtil.getMaxEnergy(stack);
		return new StringTextComponent(String.format("Energy: %d/%d RF", currentEnergy, maxEnergy));
	}
	
	public static ITextComponent getDurabilityDisplay(ItemStack stack) {
		int currentDurability = ItemUtil.getDurability(stack);
		int maxDurability = ItemUtil.getMaxDurability(stack);
		ITextComponent durabilityDisplayText = new StringTextComponent(String.format("Durability: %d/%d", currentDurability, maxDurability));
		return durabilityDisplayText;
	}
}