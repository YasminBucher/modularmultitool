package yasmin.modularmultitool.integration.jei;

import com.mojang.blaze3d.matrix.MatrixStack;
import mezz.jei.api.ingredients.IIngredientRenderer;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import yasmin.modularmultitool.recipes.MMTIngredient;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MMTIngredientRenderer implements IIngredientRenderer<MMTIngredient> {
	public IIngredientRenderer<ItemStack> renderer;
	
	@Override
	public void render(MatrixStack mstack, int x, int y, @Nullable MMTIngredient ingredient) {
		if(ingredient != null && renderer != null) {
			renderer.render(mstack, x, y, ingredient.getAcceptedItem());
		}
	}
	
	@Nonnull
	@Override
	public List<ITextComponent> getTooltip(MMTIngredient ingredient, @Nonnull ITooltipFlag iTooltipFlag) {
		return new ArrayList<>(ingredient.getAcceptedItem().getTooltip(null, iTooltipFlag));
	}
}
