package yasmin.modularmultitool.recipes;

import com.google.gson.JsonObject;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.common.crafting.IIngredientSerializer;

public class MMTIngredientSerializer implements IIngredientSerializer<MMTIngredient> {
	public static MMTIngredientSerializer INSTANCE = new MMTIngredientSerializer();
	
	@Override
	public MMTIngredient parse(PacketBuffer buffer) {
		return new MMTIngredient(buffer.readItemStack());
	}
	
	@Override
	public MMTIngredient parse(JsonObject json) {
		return MMTIngredient.deserialize(json);
	}
	
	@Override
	public void write(PacketBuffer buffer, MMTIngredient ingredient) {
		ItemStack stack = ingredient.getAcceptedItem();
		buffer.writeItemStack(stack);
	}
}