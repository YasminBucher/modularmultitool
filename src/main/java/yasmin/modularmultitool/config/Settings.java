package yasmin.modularmultitool.config;

public class Settings {
	public static float baseDamage = 0.0f;
	public static float weaponDamage = 5.0f;
	public static float weaponDamagePerLevel = 1.0f;
	public static float weaponDamageModuleAddonDamage = 2.0f;
	
	public static boolean itemMultiTool = true;
	public static boolean itemAxeModule = true;
	public static boolean itemPickaxeModule = true;
	public static boolean itemShearsModule = true;
	public static boolean itemShovelModule = true;
	public static boolean itemSwordModule = true;
	public static boolean itemAutosmeltModule = true;
	public static boolean itemDigAoeModule = true;
	public static boolean itemDigSpeedModule = true;
	public static boolean itemFortuneModule = true;
	public static boolean itemLaserRangeModule = true;
	public static boolean itemMiningPowerModule = true;
	public static boolean itemSilktouchModule = true;
	public static boolean itemWeaponDamageModule = true;
	public static boolean itemCombatLaserModule = true;
	public static boolean itemMiningLaserModule = true;
	public static boolean itemBatteryModule = true;
	
	public static int baseEnergyUsage = 50;
	public static int shearsEnergyUsage = 50;
	public static int shovelPathEnergyUsage = 50;
	public static int silktouchEnergyUsage = 100;
	public static int fortuneEnergyUsage = 50;
	public static int autosmeltEnergyUsage = 50;
	
	public static int minPermLevelInfo = 0;
	public static int minPermLevelCharge = 0;
	public static int minPermLevelRepair = 0;
}