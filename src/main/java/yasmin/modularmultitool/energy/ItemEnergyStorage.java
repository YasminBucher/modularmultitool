package yasmin.modularmultitool.energy;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.energy.IEnergyStorage;
import yasmin.modularmultitool.items.base.ItemEnergyModuleBase;
import yasmin.modularmultitool.util.ItemUtil;

public class ItemEnergyStorage implements IEnergyStorage {
	ItemStack stack;
	ItemEnergyModuleBase item;
	
	public ItemEnergyStorage(ItemStack stack, ItemEnergyModuleBase item) {
		this.stack = stack;
		this.item = item;
	}
	
	@Override
	public int receiveEnergy(int maxReceive, boolean simulate) {
		int actuallyReceived = Math.min(this.getMaxEnergyStored() - this.getCurrentEnergy(), maxReceive);

		if(!simulate) {
			this.setCurrentEnergy(Math.max(this.getCurrentEnergy() + actuallyReceived, this.getMaxEnergyStored()));
		}

		return actuallyReceived;
	}
	
	private int getCurrentEnergy() {
		CompoundNBT nbt = stack.getTag();

		if(nbt != null) {
			return nbt.getInt("mmt_CurrentEnergy");
		}

		return 0;
	}
	
	private void setCurrentEnergy(int currentEnergy) {
		if(stack.getTag() == null) {
			stack.setTag(new CompoundNBT());
		}

		stack.getTag().putInt("mmt_CurrentEnergy", currentEnergy);
	}
	
	@Override
	public int extractEnergy(int maxExtract, boolean simulate) {
		int actuallyExtracted = Math.min(maxExtract, this.getCurrentEnergy());
		
		if(!simulate) {
			this.setCurrentEnergy(Math.max(this.getCurrentEnergy() - maxExtract, 0));
		}
		
		return actuallyExtracted;
	}
	
	@Override
	public int getEnergyStored() {
		return this.getCurrentEnergy();
	}
	
	@Override
	public int getMaxEnergyStored() {
		int level = ItemUtil.getLevel(this.stack);
		return ItemUtil.getMaxEnergy(level);
	}
	
	@Override
	public boolean canExtract() {
		return false;
	}
	
	@Override
	public boolean canReceive() {
		return true;
	}
}