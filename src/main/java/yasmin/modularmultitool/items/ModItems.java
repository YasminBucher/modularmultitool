package yasmin.modularmultitool.items;

import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.Item.Properties;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import yasmin.modularmultitool.ModularMultiTool;
import yasmin.modularmultitool.blocks.ModBlocks;
import yasmin.modularmultitool.items.modules.energy.ItemBatteryModule;
import yasmin.modularmultitool.items.modules.laser.ItemCombatLaserModule;
import yasmin.modularmultitool.items.modules.laser.ItemMiningLaserModule;
import yasmin.modularmultitool.items.modules.tools.*;
import yasmin.modularmultitool.items.modules.upgrades.*;

public class ModItems {
	public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, ModularMultiTool.MODID);
	public static final RegistryObject<Item> ITEM_MULTITOOLMACHINE = ITEMS.register("item_multitoolmachine", () -> new BlockItem(ModBlocks.MULTITOOLMACHINE.get(), new Properties().group(ModularMultiTool.itemGroup)));
	public static Properties propertiesMultiTool = new Properties().setNoRepair().maxStackSize(1);
	public static final RegistryObject<Item> ITEM_MULTITOOL = ITEMS.register("item_multitool", ItemMultiTool::new);
	public static Properties propertiesModule = new Properties().maxStackSize(64).maxDamage(1).setNoRepair();
	public static final RegistryObject<Item> ITEM_MODULE_PICKAXE = ITEMS.register("item_module_tool_pickaxe", ItemPickaxeModule::new);
	public static final RegistryObject<Item> ITEM_MODULE_AXE = ITEMS.register("item_module_tool_axe", ItemAxeModule::new);
	public static final RegistryObject<Item> ITEM_MODULE_SHOVEL = ITEMS.register("item_module_tool_shovel", ItemShovelModule::new);
	public static final RegistryObject<Item> ITEM_MODULE_SWORD = ITEMS.register("item_module_tool_sword", ItemSwordModule::new);
	public static final RegistryObject<Item> ITEM_MODULE_SHEARS = ITEMS.register("item_module_tool_shears", ItemShearsModule::new);
	public static final RegistryObject<Item> ITEM_MODULE_LASER_COMBAT = ITEMS.register("item_module_laser_combat", ItemCombatLaserModule::new);
	public static final RegistryObject<Item> ITEM_MODULE_LASER_MINING = ITEMS.register("item_module_laser_mining", ItemMiningLaserModule::new);
	public static final RegistryObject<Item> ITEM_MODULE_AUTOSMELT = ITEMS.register("item_module_upgrade_autosmelt", ItemAutosmeltModule::new);
	public static final RegistryObject<Item> ITEM_MODULE_DIGAOE = ITEMS.register("item_module_upgrade_digaoe", ItemDigAoeModule::new);
	public static final RegistryObject<Item> ITEM_MODULE_DIGSPEED = ITEMS.register("item_module_upgrade_digspeed", ItemDigSpeedModule::new);
	public static final RegistryObject<Item> ITEM_MODULE_LASERRANGE = ITEMS.register("item_module_upgrade_laserrange", ItemLaserRangeModule::new);
	public static final RegistryObject<Item> ITEM_MODULE_MININGPOWER = ITEMS.register("item_module_upgrade_miningpower", ItemMiningPowerModule::new);
	public static final RegistryObject<Item> ITEM_MODULE_SILKTOUCH = ITEMS.register("item_module_upgrade_silktouch", ItemSilktouchModule::new);
	public static final RegistryObject<Item> ITEM_MODULE_WEAPONDAMAGE = ITEMS.register("item_module_upgrade_weapondamage", ItemWeaponDamageModule::new);
	public static final RegistryObject<Item> ITEM_MODULE_FORTUNE = ITEMS.register("item_module_upgrade_fortune", ItemFortuneModule::new);
	public static final RegistryObject<Item> ITEM_MODULE_BATTERY = ITEMS.register("item_module_battery", ItemBatteryModule::new);
	
	public static void registerItems() {
		ITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());
	}
}