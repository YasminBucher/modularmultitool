package yasmin.modularmultitool.inventory;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraftforge.common.util.Constants;
import yasmin.modularmultitool.items.modules.energy.ItemBatteryModule;
import yasmin.modularmultitool.items.modules.laser.ItemCombatLaserModule;
import yasmin.modularmultitool.items.modules.laser.ItemMiningLaserModule;
import yasmin.modularmultitool.items.modules.tools.*;
import yasmin.modularmultitool.items.modules.upgrades.*;

import javax.annotation.Nonnull;
import java.util.HashMap;

public class MultiToolInventory implements IInventory {
	public static final int INV_SIZE = 16;
	private final ItemStack invItem;
	
	private ItemStack[] inventory = new ItemStack[INV_SIZE];
	private HashMap<Integer, Class<?>> slotModuleMap = new HashMap<>();
	
	public MultiToolInventory(ItemStack invItem) {
		this.initSlotModuleMap();
		this.invItem = invItem;
		
		if(!invItem.hasTag()) {
			invItem.setTag(new CompoundNBT());
		}
		
		this.readFromNBT(invItem.getTag());
	}
	
	@SuppressWarnings("MagicNumber")
	private void initSlotModuleMap() {
		this.slotModuleMap.put(0, ItemPickaxeModule.class);
		this.slotModuleMap.put(1, ItemAxeModule.class);
		this.slotModuleMap.put(2, ItemShovelModule.class);
		this.slotModuleMap.put(3, ItemSwordModule.class);
		this.slotModuleMap.put(4, ItemShearsModule.class);
		
		this.slotModuleMap.put(5, ItemMiningLaserModule.class);
		this.slotModuleMap.put(6, ItemCombatLaserModule.class);
		
		this.slotModuleMap.put(7, ItemBatteryModule.class);
		
		this.slotModuleMap.put(8, ItemAutosmeltModule.class);
		this.slotModuleMap.put(9, ItemDigAoeModule.class);
		this.slotModuleMap.put(10, ItemDigSpeedModule.class);
		this.slotModuleMap.put(11, ItemFortuneModule.class);
		this.slotModuleMap.put(12, ItemLaserRangeModule.class);
		this.slotModuleMap.put(13, ItemMiningPowerModule.class);
		this.slotModuleMap.put(14, ItemSilktouchModule.class);
		this.slotModuleMap.put(15, ItemWeaponDamageModule.class);
	}
	
	public void readFromNBT(CompoundNBT compound) {
		if(compound != null) {
			ListNBT items = compound.getList("MultiToolModules", Constants.NBT.TAG_COMPOUND);
			
			for(int i = 0; i < items.size(); i++) {
				CompoundNBT item = items.getCompound(i);
				int slot = item.getInt("ModuleSlot");
				
				if(slot >= 0 && slot < this.getSizeInventory()) {
					ItemStack itemStack = ItemStack.read(item);
					
					if(this.isItemValidForSlot(slot, itemStack)) {
						this.inventory[slot] = itemStack;
					}
				}
			}
		}
	}
	
	@Override
	public int getSizeInventory() {
		return this.inventory.length;
	}
	
	@Override
	public boolean isEmpty() {
		boolean isEmpty = true;
		
		for(int i = 0; i < INV_SIZE; i++) {
			if(!this.inventory[i].isEmpty()) {
				isEmpty = false;
				break;
			}
		}
		
		return isEmpty;
	}
	
	@Nonnull
	@Override
	public ItemStack getStackInSlot(int index) {
		if(this.inventory[index] == null) {
			this.inventory[index] = ItemStack.EMPTY;
		}
		
		return this.inventory[index];
	}
	
	@Nonnull
	@Override
	public ItemStack decrStackSize(int index, int count) {
		ItemStack stack = this.getStackInSlot(index);
		
		if(!stack.isEmpty()) {
			if(stack.getCount() > count) {
				stack = stack.split(count);
				this.markDirty();
			} else {
				this.setInventorySlotContents(index, ItemStack.EMPTY);
			}
		}
		
		return stack;
	}
	
	@Nonnull
	@Override
	public ItemStack removeStackFromSlot(int index) {
		ItemStack stack = this.getStackInSlot(index);
		
		if(!stack.isEmpty()) {
			this.setInventorySlotContents(index, ItemStack.EMPTY);
		}
		
		return stack;
	}
	
	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		this.inventory[index] = stack;
		
		if(!stack.isEmpty() && stack.getCount() > this.getInventoryStackLimit()) {
			stack.setCount(this.getInventoryStackLimit());
		}
		
		this.markDirty();
	}
	
	@Override
	public void markDirty() {
		this.writeToNBT(this.invItem.getTag());
	}
	
	@Override
	public boolean isUsableByPlayer(@Nonnull PlayerEntity player) {
		return true;
	}
	
	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack) {
		return stack.getItem().getClass() == this.slotModuleMap.get(index);
	}
	
	public void writeToNBT(CompoundNBT compound) {
		ListNBT items = new ListNBT();
		
		for(int i = 0; i < this.getSizeInventory(); i++) {
			if(!this.getStackInSlot(i).isEmpty()) {
				CompoundNBT item = new CompoundNBT();
				item.putInt("ModuleSlot", i);
				this.getStackInSlot(i).write(item);
				
				items.add(item);
			}
		}
		
		compound.put("MultiToolModules", items);
	}
	
	@Override
	public void clear() {
		for(int i = 0; i < this.getSizeInventory(); i++) {
			this.inventory[i] = ItemStack.EMPTY;
		}
	}
}