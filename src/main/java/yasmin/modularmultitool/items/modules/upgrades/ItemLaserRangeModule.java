package yasmin.modularmultitool.items.modules.upgrades;

import yasmin.modularmultitool.config.Settings;
import yasmin.modularmultitool.items.base.ItemUpgradeModuleBase;

public class ItemLaserRangeModule extends ItemUpgradeModuleBase {
	public ItemLaserRangeModule() {
		super();
	}
	
	@Override
	public boolean isEnabled() {
		return Settings.itemLaserRangeModule;
	}
}