package yasmin.modularmultitool.items.base;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import yasmin.modularmultitool.ModularMultiTool;
import yasmin.modularmultitool.items.ModItems;
import yasmin.modularmultitool.util.ItemUtil;

public class ItemModuleBase extends ItemBase {
	public ItemModuleBase() {
		this(ModItems.propertiesModule);
	}
	
	public ItemModuleBase(Properties properties) {
		super(properties);
	}
	
	@Override
	public void fillItemGroup(ItemGroup group, NonNullList<ItemStack> items) {
		if(this.isInGroup(group)) {
			for(int i = 0; i <= this.getMaxLevel(); i++) {
				ItemStack stackWithLevel = new ItemStack(this);
				ItemUtil.setLevel(stackWithLevel, i);
				items.add(stackWithLevel);
			}
		}
	}
	
	public int getMaxLevel() {
		return 0;
	}
}